#-------------------------------------------------
#
# Project created by QtCreator 2014-08-20T19:59:58
#
#-------------------------------------------------

QT       += core gui widgets webengine



QMAKE_CXXFLAGS += -std=c++0x

TARGET = ForgottenLegend_LifeInTheEmpire_TheCaseOfTheEmpire
TEMPLATE = app

SOURCES += \
    Src/main.cpp \
    Src/Interface/DockWidget.cpp \
    Src/Interface/InputDialog.cpp \
    Src/Interface/MainWindow.cpp \
    Src/Interface/TableAction.cpp \
    Src/Game/Hero.cpp \
    Src/Game/Horse.cpp \
    Src/Game/Locations.cpp \
    Src/Game/Npcs.cpp \
    Src/Game/PreHistory.cpp \
    Src/Game/Settings.cpp \
    Src/Core/GameBase/Apartment.cpp \
    Src/Core/GameBase/ConvertToHtml.cpp \
    Src/Core/GameBase/GameObject.cpp \
    Src/Core/GameBase/Human.cpp \
    Src/Core/GameBase/Humanoid.cpp \
    Src/Core/GameBase/HumanWingMob.cpp \
    Src/Core/GameBase/Mob.cpp \
    Src/Core/GameBase/Money.cpp \
    Src/Core/GameBase/Picture.cpp \
    Src/Core/GameBase/Place.cpp \
    Src/Core/GameBase/Room.cpp \
    Src/Core/GameBase/SuperNaturalMob.cpp \
    Src/Core/GameBase/Transport.cpp \
    Src/Core/GameBase/WingMob.cpp \
    Src/Core/GameBase/Work.cpp \
    Src/Core/Date.cpp \
    Src/Core/OldDate.cpp \
    Src/Core/Weather.cpp \
    Src/Game/Bedroom.cpp \
    Src/Game/Kitchen.cpp \
    Src/Game/Corridor.cpp \
    Src/Interface/AnimatedTextBrowser.cpp \
    Src/Core/GameBase/Clothes.cpp \
    Src/Game/Place/Bed.cpp \
    Src/Interface/MyTextBrowser.cpp \
    Src/Core/OldWeather.cpp \
    Src/Game/Calculations.cpp \
    Src/Game/Place/Yard.cpp \
    Src/Game/Place/Locker.cpp \
    Src/Core/GameBase/Item.cpp \
    Src/Core/GameBase/Shoes.cpp \
    Src/Game/Item/Clothes/Sarafan.cpp \
    Src/Game/Item/Shoes/FlipFlop.cpp \
    Src/Game/Items.cpp

FORMS += \
    Src/Interface/InputDialog.ui \
    Src/Interface/MainWindow.ui

HEADERS += \
    Src/Interface/DockWidget.h \
    Src/Interface/InputDialog.h \
    Src/Interface/MainWindow.h \
    Src/Interface/TableAction.h \
    Src/Game/Hero.h \
    Src/Game/Horse.h \
    Src/Game/Locations.h \
    Src/Game/Npcs.h \
    Src/Game/PreHistory.h \
    Src/Game/Settings.h \
    Src/Core/GameBase/Apartment.h \
    Src/Core/GameBase/ConvertToHtml.h \
    Src/Core/GameBase/GameObject.h \
    Src/Core/GameBase/Human.h \
    Src/Core/GameBase/Humanoid.h \
    Src/Core/GameBase/HumanWingMob.h \
    Src/Core/GameBase/Mob.h \
    Src/Core/GameBase/Money.h \
    Src/Core/GameBase/Picture.h \
    Src/Core/GameBase/Place.h \
    Src/Core/GameBase/Room.h \
    Src/Core/GameBase/SuperNaturalMob.h \
    Src/Core/GameBase/Transport.h \
    Src/Core/GameBase/WingMob.h \
    Src/Core/GameBase/Work.h \
    Src/Core/Date.h \
    Src/Core/OldDate.h \
    Src/Core/Weather.h \
    Src/Game/Bedroom.h \
    Src/Game/Kitchen.h \
    Src/Game/Corridor.h \
    Src/Interface/AnimatedTextBrowser.h \
    Src/Core/GameBase/Clothes.h \
    Src/Game/Place/Bed.h \
    Src/Interface/MyTextBrowser.h \
    Src/Core/OldWeather.h \
    Src/Game/Calculations.h \
    Src/Game/Place/Yard.h \
    Src/Game/Place/Locker.h \
    Src/Core/GameBase/Item.h \
    Src/Core/GameBase/Shoes.h \
    Src/Game/Item/Clothes/Sarafan.h \
    Src/Game/Item/Shoes/FlipFlop.h \
    Src/Game/Items.h
