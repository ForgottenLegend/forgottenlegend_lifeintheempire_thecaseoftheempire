#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

#include "Interface/MainWindow.h"

Hero *hero;
OldDate *date;

int main(int argc, char *argv[])
{
    srand(time(0));


    QApplication a(argc, argv);

    QString locale = QLocale::system().name();
    QTranslator *qtTranslator = new QTranslator(qApp);
    qtTranslator->load("qt_" + locale, "translations");
    qApp->installTranslator(qtTranslator);

    QTranslator *appTranslator = new QTranslator(qApp);
    appTranslator->load(QString("${TRANSLATIONS_DIR}/${PROJECT_NAME}_%1.qm").arg(locale));
    qApp->installTranslator(appTranslator);

    QTranslator *translator = new QTranslator(qApp);
    translator->load("ForgottenLegend_LifeInTheEmpire_TheCaseOfTheEmpire_" + locale, "translations");
    qApp->installTranslator(translator);

    QTranslator *Translator = new QTranslator(qApp);
    Translator->load("qt_" + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    qApp->installTranslator(Translator);

    MainWindow w;
    w.show();

    return a.exec();
}

/*  ГГ — дочь известного бандита и поствавщика охраников для смотрящего.
 *  ГГ единственный ребёнок, мать умерла при родах, а из-за мутаций вероятность рождения 2-го крайне мала.
 * Отец узнал, что скоро произойдёт очередное расширение районов и ему придётся уплатить сумму, набранную 2-мя покалениями за
 * переест, что лешит возможности отправить своё чадо в нормальный район. И он рискнул.
 *  Отдав все деньги, которые собирались на протяжении 10 покалениы смотрящему и пообещав бесплатно поставлять охраников в
 * течении 5 лет, он договорился на транспортировку своего чада в нормальный район и подбор жилища ей.
 *  Ведь она единственная надежда продлить род, ведь она ещё девственница и не известно может она иметь детей или нет.
 */

/*  Смотрящий выполнил условия сделки, доставил Роксану Воиборовну в нормальный район и устроил её в местный институт всего, чем
 * обеспечил её жильём (общежитием).
 *  Перед растованием смотрящие предупредил о том, что никто не должен догадаться то, что Рока из бедного района.
 *  Комендант заселил в двушку, но в момент заселения соседа не было в ней, так что знакомство ещё предстоит…
 *  Однако, нормальный район, по нравственным принципам оказался не так далёк от бедного…
 */
