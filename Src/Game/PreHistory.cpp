#include "PreHistory.h"

extern Hero *hero;
extern OldDate *date;
extern TableAction *tableAction;
Calculations *calc;

PreHistory::PreHistory(MainWindow *mw, Ui::MainWindow *ui, Place *parent) :
    Place(mw,ui,parent)
{
    createNpc();
    stage = 0;
    date = new OldDate(5, 0, 0, 0, 7, 1500);
    calc = new Calculations(date, hero);
}

void PreHistory::entry(Place *parent)
{
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    connect(ui->tableWidgetAction,&TableAction::actionClicked,this,&PreHistory::click);
    connect(mw, &MainWindow::repaint, this, &PreHistory::repaint);

    Q_UNUSED(parent);
    mw->addAction (tr("Встать"), 1);

    text();
}

void PreHistory::quit()
{
    disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    disconnect(ui->tableWidgetAction,&TableAction::actionClicked,this,&PreHistory::click);
    disconnect(mw, &MainWindow::repaint, this, &PreHistory::repaint);
}

void PreHistory::anchorClicked(QUrl arg1)
{
    Q_UNUSED(arg1);
}

void PreHistory::click(int index)
{
    Q_UNUSED(index);
    if (stage < 5)
    {
        ui->tableWidgetAction->setRowCount(0);
        ++stage;
        if (stage == 1)
        {
            mw->addAction(tr("Прочитать записку"), 1);
            date->addHouring(5);
        }
        else
            if (stage == 2)
            {
                mw->addAction(tr("Положить записку"), 1);
                date->addHouring(5);
            }
            else
                if (stage == 3)
                {
                    mw->addAction(tr("Прилечь на кровать"), 1);
                    date->addHouring(5);
                }
                else
                    if (stage == 4)
                    {
                        mw->addAction(tr("Проснуться и попытаться вспомнить что-то из прошлого"), 1);
                        date->addHouring(5);
                    }
                    else
                        if (stage == 5)
                        {
                            mw->addAction(tr("Окунуться в настоящее"), 1);
                            hero->setSleep(1);
                            date->addHouring(500);
                        }
        text();
    }
    else
    {
        hero->setSleep(0);
        mw->displayAdditionalDec();

        createLocation(mw, ui);
        createItems();

        quit();
        bed->entry(bedroom);

        ui->save->setEnabled(1);
    }
}

void PreHistory::text()
{
    ui->textBrowser->clear();
    QString qs;
    switch (stage)
    {
    case 0:
        qs.append(ConvertToHtml::getHeader(tr("Кровать")));
        qs.append(Picture(QString("pic/%1/wakeUpNude.jpg").arg(hero->getPicPrefix())).getStr());
        /*qs.append(ConvertToHtml::getParagraph(
                      ConvertToHtml::getFont(
                          ConvertToHtml::getColorStr(tr("\"Бля… Как же болит голова… Кто я? Где я? Ничего не помню…\
                                                        Что же со мной было?…\""), ConvertToHtml::Color::Green),
                                                        (char)ConvertToHtml::Font::Italic),
                                                     (char)ConvertToHtml::Margin::Top));*/

        ui->textBrowser->setHtml(qs);
        ui->textBrowser->append(ConvertToHtml::getParagraph(
                                    ConvertToHtml::getFont(
                                        ConvertToHtml::getColorStr(tr("\"Бля… Как же болит голова… Кто я? Где я? Ничего не помню…\
 Что же со мной было?…\""), ConvertToHtml::Color::Green),
                                                                      (char)ConvertToHtml::Font::Italic),
                                                                   (char)ConvertToHtml::Margin::Top));
        break;
    case 1:
        qs.append(ConvertToHtml::getHeader(tr("Комната")));
        qs.append(Picture("pic/location/myHouse/rooms/kitchen.jpg").getStr());
        qs.append(ConvertToHtml::getParagraph(
                      ConvertToHtml::getFont(
                          ConvertToHtml::getColorStr(
                              tr("\"Как же мне хреново… Лучше было бы не вставать…\""),
                              ConvertToHtml::Color::Green),
                          (char)ConvertToHtml::Font::Italic),
                      (char)ConvertToHtml::Margin::Top));
        qs.append(ConvertToHtml::getParagraph(tr("Ты замечаешь записку на столе."), (char)ConvertToHtml::Margin::Bottom));

        ui->textBrowser->setHtml(qs);
        break;
    case 2:
        qs.append(ConvertToHtml::getHeader(tr("Записка")));
        qs.append(Picture("pic/preHistory/note.jpg").getStr());
        qs.append("<span style=\"color:purple\">");
        qs.append(ConvertToHtml::getParagraph(tr("\"…Зря я не послушалась колдуньи…")));
        qs.append(ConvertToHtml::getParagraph(tr("…Я проклята…")));
        qs.append(ConvertToHtml::getParagraph(
                      tr("…Всё меньше контролирую своё тело… всё больше я чувствую себя младенцем-мальчиком…")));
        qs.append(ConvertToHtml::getParagraph(tr("…Меня зовут %1, по отцу — %2…").arg(hero->getName(), hero->getSecondName())));
        qs.append(ConvertToHtml::getParagraph(tr("…Мой дом находится на отшибе деревни…")));
        qs.append(ConvertToHtml::getParagraph(
                      tr("…Деревня окружена болотами, поэтому линия фронта находится вокруг, а по деревне проходит редко…")));
        qs.append(ConvertToHtml::getParagraph(tr("…Время между моим контролем моего тела возрастает…")));
        qs.append(ConvertToHtml::getParagraph(tr("…Я прихожу в себя в случайных местах…")));
        qs.append(ConvertToHtml::getParagraph(tr("…Видимо это последняя запись…\"")));
        qs.append("</span>");

        ui->textBrowser->setHtml(qs);
        break;
    case 3:
        qs.append(ConvertToHtml::getHeader(tr("Спальня")));
        qs.append(Picture("pic/location/myHouse/rooms/bedroom.jpg").getStr());
        qs.append(ConvertToHtml::getParagraph(
                      ConvertToHtml::getFont(
                          ConvertToHtml::getColorStr(tr("\"Что-то мне не хорошо… Надо прилечь\""),
                                                     ConvertToHtml::Color::Green),
                          (char)ConvertToHtml::Font::Italic)));

        ui->textBrowser->setHtml(qs);
        break;
    case 4:
        qs.append(ConvertToHtml::getHeader(tr("Сон")));
        qs.append(Picture("pic/preHistory/babka.jpg").getStr());
        qs.append(ConvertToHtml::getParagraph(
                      tr("%1, — сказала старуха. %2").
                      arg(ConvertToHtml::getColorStr(
                              tr("— Слушай сюды, солдатик"), ConvertToHtml::Color::Red)).
                      arg(ConvertToHtml::getColorStr(
                              tr("— Ты всё равно ничего не сможешь сделать. Я не смогла, в своё время, и ты не сможешь. Это цикл древен, как мир. Не переживай, шанс вернуться в твоё тело тебе ещё представиться. Может быть…"),
                              ConvertToHtml::Color::Red)), (char)ConvertToHtml::Margin::Top));
        qs.append(ConvertToHtml::getParagraph(tr("После этих слов старуха начала что-то говорить…")));

        ui->textBrowser->setHtml(qs);
        break;
    case 5:
        qs.append(ConvertToHtml::getHeader(tr("Воспоминание")));
        qs.append(Picture("pic/successor/successor.jpg").getStr());
        qs.append(ConvertToHtml::getParagraph(
                      ConvertToHtml::getColorStr(
                          ConvertToHtml::getFont(
                              tr("\"Эта девушка… Она была очень важна для меня… Но кто она?\""),
                              (char)ConvertToHtml::Font::Italic), ConvertToHtml::Color::Green)));

        ui->textBrowser->setHtml(qs);
        break;
    default:
        break;
    }
}
