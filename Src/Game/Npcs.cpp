#include "Npcs.h"

void createNpc()
{
    if (hero)
        delete hero;

    hero = new Hero();
    hero->setAge(18);
    hero->setApparentAge(16);
    hero->setDominate(-200);
    hero->setGender(Hero::Gender::Female);
    hero->setId(0);
    hero->setName(QT_TR_NOOP("Роксана"));
    hero->setMoral(1000);
    hero->setObjectName("Roksana");
    hero->setReputation(0);
    hero->setSecondName(QT_TR_NOOP("Воиборовна"));
    hero->setShortName(QT_TR_NOOP("Рока"));
    hero->setPicPrefix("roksana");
}
