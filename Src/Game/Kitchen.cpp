#include "Kitchen.h"

Kitchen::Kitchen(MainWindow *mw, Ui::MainWindow *ui, QObject *parent) :
    Room(mw, ui, parent)
{
    name = tr("Кухня");
    stage = 0;

    fistWindow = 1;
}

void Kitchen::entry(Place *parent)
{
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    connect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    connect(mw, SIGNAL(repaint()), this, SLOT(repaint()));

    if (parent)
        this->setParent(parent);

    text();
}

void Kitchen::quit()
{
    /*qDebug() << "Kitchen";
    qDebug() << disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    qDebug() << disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    qDebug() << disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));*/

    disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));
}

void Kitchen::anchorClicked(QUrl arg1)
{
    QString qs = arg1.toDisplayString().toLower();
    if (qs == "window")
    {
        ui->tableWidgetAction->setRowCount(0);
        if (fistWindow)
        {
            fistWindow = 0;

            qs.clear();
            qs.append(ConvertToHtml::getHeader(tr("Улица")));
            qs.append(Picture("pic/location/myHouse/rooms/destroyedVillages.jpg").getStr());
            qs.append(ConvertToHtml::getParagraph(tr("%1 — подумали вы.").
                                                  arg(ConvertToHtml::getFont(
                                                          ConvertToHtml::getColorStr(
                                                              tr("\"О, Создатель… Что же сдесь произошло.\""),
                                                              ConvertToHtml::Color::Green),
                                                          char(ConvertToHtml::Font::Italic)|(char)ConvertToHtml::Font::Bold))));
            mw->addAction(tr("Отойти от окна"), 418);

            ui->textBrowser->setHtml(qs);
        }
        else
        {
            qs.clear();
            qs.append(ConvertToHtml::getHeader(tr("Улица")));
            qs.append(Picture("pic/location/myHouse/rooms/destroyedVillages.jpg").getStr());
            qs.append(ConvertToHtml::getParagraph(tr("За окном всё та же удручающая обстановка.")));
            mw->addAction(tr("Отойти от окна"), 418);

            ui->textBrowser->setHtml(qs);
        }
    }
}

void Kitchen::click(int index)
{
    switch (index)
    {
    case 1:
        quit();
        static_cast<Room*>(parent())->entry();
        date->addHouring(1);
        break;
    case 2:
        quit();
        static_cast<Room*>(parent()->children().at(0))->entry();
        date->addHouring(1);
        break;
    case 418:
        this->text();
        break;
    default:
        qDebug() << index;
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
        break;
    }
}

void Kitchen::text()
{
    ui->textBrowser->clear();
    ui->tableWidgetAction->setRowCount(0);
    QString qs;

    qs.append(tr("%1%2").
              arg(ConvertToHtml::getHeader(name)).
              arg(Picture("pic/location/myHouse/rooms/kitchen.jpg").getStr()));
    switch (stage)
    {
    case 0:
        qs.append(ConvertToHtml::getParagraph(tr("На кухне стоит печь, под которой находится несколько дров. На против печи стоит старый стол.")));
        qs.append(ConvertToHtml::getParagraph(
                      ConvertToHtml::getLink(tr("Посмотреть в окно"), "window")));
        mw->addAction(tr("Пойти в спальню"), 2);
        mw->addAction (tr("Выйти в коридор"), 1);
        break;
    default:
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
        break;
    }

    ui->textBrowser->setHtml(qs);
}
