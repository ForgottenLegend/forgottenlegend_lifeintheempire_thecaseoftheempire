#ifndef HORSE_H
#define HORSE_H

#include "../Core/GameBase/Transport.h"
#include "../Core/GameBase/Mob.h"

class Horse : public Transport, public Mob
{
public:
    Horse();
};

#endif // HORSE_H
