#ifndef YARD_H
#define YARD_H

#include <QScrollBar>

#include "../../Core/GameBase/Room.h"
#include "../../Interface/AnimatedTextBrowser.h"

class Yard : public Room
{
    Q_OBJECT
public:
    explicit Yard(MainWindow *mw, Ui::MainWindow *ui, Room *parent=0);

    void entry(Place *parent = 0);

    void quit();

signals:

protected slots:
    void anchorClicked(QUrl arg1);

    void click(int index);

    void text();

public slots:
};

#endif // YARD_H
