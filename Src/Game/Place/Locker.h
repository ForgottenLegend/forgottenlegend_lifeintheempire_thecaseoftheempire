#ifndef LOCKER_H
#define LOCKER_H

#include "../../Core/GameBase/Place.h"

class Locker : public Place
{
    Q_OBJECT
public:
    explicit Locker(MainWindow *mw, Ui::MainWindow *ui, Place *parent = 0);

    void entry(Place *parent);

    void quit();

signals:

protected slots:
    void anchorClicked(QUrl arg1);

    void click(int index);

    void text();
};

#endif // LOCKER_H
