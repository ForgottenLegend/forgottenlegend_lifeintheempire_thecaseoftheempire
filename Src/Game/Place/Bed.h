#ifndef BED_H
#define BED_H

#include "../../Core/GameBase/Place.h"

extern Hero *hero;

class Bed : public Place
{
    Q_OBJECT
public:
    explicit Bed(MainWindow *mw, Ui::MainWindow *ui, Place *parent = 0);

    void entry(Place *parent);

    void quit();

signals:

protected slots:
    void anchorClicked(QUrl arg1);

    void click(int index);

    void text();

private:
    char stage;
};

#endif // BED_H
