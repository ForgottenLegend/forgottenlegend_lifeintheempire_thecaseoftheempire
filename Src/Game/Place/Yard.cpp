#include "Yard.h"

Yard::Yard(MainWindow *mw, Ui::MainWindow *ui, Room *parent) :
    Room(mw, ui, parent)
{
    name = tr("Двор");
    stage = 0;
}

void Yard::entry(Place *parent)
{
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    connect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    connect(mw, SIGNAL(repaint()), this, SLOT(repaint()));

    if (parent)
        this->setParent(parent);

    text();
}

void Yard::quit()
{
    disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));
}

void Yard::anchorClicked(QUrl arg1)
{
    QString qs = arg1.toDisplayString().toLower();
    if (qs == "zombi")
    {
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
    }
    else
    {
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
    }
}

void Yard::click(int index)
{
    switch (index)
    {
    case 0:
//        ++stage;
        repaint();
        break;
    case 1:
        /*quit();
        dynamic_cast<Room*>(parent())->entry();
        date->addHouring(2);*/

        break;
    case 2:
        quit();
        dynamic_cast<Room*>(children().at(0))->entry();
        date->addHouring(1);

        break;
    default:
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
        break;
    }
}

void Yard::text()
{
    ui->tableWidgetAction->setRowCount(0);

    AnimatedTextBrowser *qtb = ui->textBrowser;
    switch (stage)
    {
    case 0:
        qtb->setHtml(ConvertToHtml::getHeader(name));
        qtb->append(Picture("pic/location/myHouse/rooms/yard.jpg").getStr());
        qtb->append(ConvertToHtml::getParagraph(
                        ConvertToHtml::getColorStr(
                            ConvertToHtml::getFont(tr("\"Хммм, дворик выгляжит совсем не ухоженным…\""),
                                                   (char)ConvertToHtml::Font::Italic),
                            ConvertToHtml::Color::Green), (char)ConvertToHtml::Margin::Top));
        qtb->append(ConvertToHtml::getParagraph(tr("Ты замечаешь %1.").
                                                arg(ConvertToHtml::getLink(tr("странного человека"), "zombi")),
                                                (char)ConvertToHtml::Margin::Bottom));
        mw->addAction(tr("Вернуться в дом"), 2);
//        qDebug() << qtb->toHtml();
        break;
    default:
        break;
    }
}
