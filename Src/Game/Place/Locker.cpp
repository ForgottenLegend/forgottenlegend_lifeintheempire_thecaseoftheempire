#include "Locker.h"

Locker::Locker(MainWindow *mw, Ui::MainWindow *ui, Place *parent) :
    Place(mw, ui, parent)
{
    name = tr("Шкаф");
}

void Locker::entry(Place *parent)
{
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    connect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    connect(mw, SIGNAL(repaint()), this, SLOT(repaint()));

    this->setParent(parent);

    text();
}

void Locker::quit()
{
    /*qDebug() << "Bed";
    qDebug() << disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    qDebug() << disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    qDebug() << disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));*/

    disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));
}

void Locker::anchorClicked(QUrl arg1)
{

}

void Locker::click(int index)
{
    switch (index)
    {
    case 1:
        quit();
        static_cast<Place *>(this->parent())->entry();
        date->addHouring(1);
    default:
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
        break;
    }
}

void Locker::text()
{
    ui->tableWidgetAction->setRowCount(0);

    QString qs;

    mw->addAction(tr("Отойти от шкафа"), 1);

    qs.append(ConvertToHtml::getHeader("%1").arg(name));
    qs.append(Picture("pic/location/locker.jpg").getStr());

    qs.append(ConvertToHtml::getParagraph(
                  tr("В шкайу имеется слудующая одежда")));

    ui->textBrowser->setHtml(qs);
}
