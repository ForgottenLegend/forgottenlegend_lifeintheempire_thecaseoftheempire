#ifndef PREHISTORY_H
#define PREHISTORY_H

#include "../Core/GameBase/Place.h"
#include "../Core/GameBase/Picture.h"
#include "../Core/GameBase/ConvertToHtml.h"
#include "../Interface/MainWindow.h"
#include "ui_MainWindow.h"
#include <QMessageBox>

#include "Npcs.h"
#include "../Core/OldDate.h"
#include "Locations.h"
#include "Calculations.h"
#include "Items.h"

extern Hero *hero;

extern Calculations *calc;

class MainWindow;
namespace Ui {
class MainWindow;
}

class PreHistory : public Place
{
public:
    Q_OBJECT
public:
    explicit PreHistory(MainWindow* mw, Ui::MainWindow* ui,Place *parent = 0);

    void entry(Place *parent = 0);

    void quit();

signals:

protected slots:
    void anchorClicked(QUrl arg1);

    void click(int index);

    void text();
private:
    char stage;
};

#endif // PREHISTORY_H
