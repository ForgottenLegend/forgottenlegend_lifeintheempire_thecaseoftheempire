#ifndef FLIPFLOP_H
#define FLIPFLOP_H

#include "../../../Core/GameBase/Shoes.h"

class FlipFlop : public Shoes
{
public:
    FlipFlop(QString pic);
    FlipFlop(const FlipFlop *FlipFlop);
};

#endif // FLIPFLOP_H
