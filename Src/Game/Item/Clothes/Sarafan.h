#ifndef SARAFAN_H
#define SARAFAN_H

#include "../../../Core/GameBase/Clothes.h"

class Sarafan : public Clothes
{
public:
    Sarafan(QString picFront, bool part = false, QString picBack = "");
    Sarafan(const Sarafan *sarafan);
};

#endif // SARAFAN_H
