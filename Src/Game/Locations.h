#ifndef LOCATIONS_H
#define LOCATIONS_H

#include <map>

#include "Bedroom.h"
#include "Kitchen.h"
#include "Corridor.h"
#include "Place/Bed.h"
#include "Place/Yard.h"

extern Bedroom *bedroom;
extern Kitchen *kitchen;
extern Corridor *corridor;
extern Bed *bed;
extern Yard *yard;

void createLocation(MainWindow *mw, Ui::MainWindow *ui);

#endif // LOCATIONS_H
