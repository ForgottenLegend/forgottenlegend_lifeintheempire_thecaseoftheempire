#ifndef ITEMS_H
#define ITEMS_H

#include <map>

#include <QDebug>

#include "Item/Clothes/Sarafan.h"
#include "Item/Shoes/FlipFlop.h"

extern std::map<int, Clothes *> clothes_s;
extern std::map<int, Shoes *> shoes_s;

void createItems();

#endif // ITEMS_H
