#include "Locations.h"

Bedroom *bedroom;
Kitchen *kitchen;
Corridor *corridor;
Bed *bed;
Yard *yard;

std::map<int, Place *> locations;

void createLocation(MainWindow *mw, Ui::MainWindow *ui)
{
    //Очистка
    {
        for (std::map<int, Place*>::iterator i = locations.begin(),
             end = locations.end(); i!= end; ++i)
        {
            delete i->second;
        }
    }
    yard = new Yard(mw, ui);
    yard->setId(4);
    locations[4] = yard;

    corridor = new Corridor(mw, ui, yard);
    corridor->setId(3);
    locations[3] = corridor;

    bedroom = new Bedroom(mw, ui, corridor);
    bedroom->setId(1);
    locations[1] = bedroom;

    kitchen = new Kitchen(mw, ui, corridor);
    kitchen->setId(2);
    locations[2] = kitchen;

    bed = new Bed(mw, ui);
    bed->setId(-1);
    locations[-1] = bed;
}
