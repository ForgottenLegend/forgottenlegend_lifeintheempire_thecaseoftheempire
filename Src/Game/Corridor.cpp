#include "Corridor.h"

Corridor::Corridor(MainWindow *mw, Ui::MainWindow *ui, QObject *parent) :
    Room(mw, ui, parent)
{
    name = tr("Коридор");
    stage = 0;
}

void Corridor::entry(Place *parent)
{
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    connect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    connect(mw, SIGNAL(repaint()), this, SLOT(repaint()));

    if (parent)
        this->setParent(parent);

    text();
}

void Corridor::quit()
{
    /*qDebug() << "Corridor";
    qDebug() << disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    qDebug() << disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    qDebug() << disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));*/

    disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));
}

void Corridor::anchorClicked(QUrl arg1)
{
    QString qs = arg1.toDisplayString().toLower();
    if (qs == "locker")
        QMessageBox::information(0, tr("Заглушка"), tr("Вы нашли шкаф"));
    else
        if (qs == "mirror")
            QMessageBox::information(0, tr("Заглушка"), tr("Вы нашли зеркало"));
}

void Corridor::click(int index)
{
    switch (index)
    {
    case 1:
        quit();
        static_cast<Room*>(parent())->entry();
        date->addHouring(5);
        break;
    case 2:
        quit();
        static_cast<Room*>(children().at(1))->entry();
        date->addHouring(1);

        break;
    case 3:
        quit();
        static_cast<Room*>(children().at(0))->entry();
        date->addHouring(2);
        break;
    default:
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
        break;
    }
}

void Corridor::text()
{
    ui->textBrowser->clear();
    ui->tableWidgetAction->setRowCount(0);
    QString qs;

    qs.append(tr("%1%2").
              arg(ConvertToHtml::getHeader(name)).
              arg(Picture("pic/location/myHouse/rooms/corridor.jpg").getStr()));
    switch (stage)
    {
    case 0:
        qs.append(ConvertToHtml::getParagraph(tr("В коридоре располагается %1 и %2.").
                                              arg(ConvertToHtml::getLink(tr("шкаф"), "Locker")).
                                              arg(ConvertToHtml::getLink(tr("зеркало"), "mirror"))));
        mw->addAction(tr("Пойти на кухню"), 2);
        mw->addAction(tr("Пойти в спальню"), 3);

        if (hero->getClothes() != Cloth::nude)
            mw->addAction(tr("Выйти во двор"), 1);
        else
            qs.append(ConvertToHtml::getHeader(ConvertToHtml::getColorStr(tr("НЕЛЬЗЯ ВЫЙТИ ГОЛОЙ ИЗ ДОМА"),
                                                                          ConvertToHtml::Color::Red)));
        break;
    default:
        break;
    }

    ui->textBrowser->setHtml(qs);
}
