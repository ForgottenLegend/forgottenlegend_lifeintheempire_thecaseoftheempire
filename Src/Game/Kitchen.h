#ifndef KITCHEN_H
#define KITCHEN_H

#include "../Core/GameBase/Room.h"
#include "../Core/GameBase/ConvertToHtml.h"
#include "../Core/GameBase/Picture.h"
#include "../Core/OldDate.h"

extern OldDate *date;

class Kitchen : public Room
{
    Q_OBJECT
public:
    explicit Kitchen(MainWindow *mw, Ui::MainWindow *ui, QObject *parent = 0);

    void entry(Place *parent = 0);

    void quit();

signals:

protected slots:
    void anchorClicked(QUrl arg1);

    void click(int index);

    void text();

public slots:

private:
    bool fistWindow;
};

#endif // KITCHEN_H
