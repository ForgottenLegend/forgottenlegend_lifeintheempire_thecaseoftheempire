#ifndef CORRIDOR_H
#define CORRIDOR_H

#include <QScrollBar>

#include "../Core/GameBase/Room.h"
#include "../Core/GameBase/ConvertToHtml.h"
#include "../Core/GameBase/Picture.h"

extern Hero *hero;

class Corridor : public Room
{
    Q_OBJECT
public:
    explicit Corridor(MainWindow *mw, Ui::MainWindow *ui, QObject *parent = 0);

    void entry(Place *parent = 0);

    void quit();

signals:

protected slots:
    void anchorClicked(QUrl arg1);

    void click(int index);

    void text();

public slots:

};

#endif // CORRIDOR_H
