#include "Items.h"

std::map<int, Clothes *> clothes_s;
std::map<int, Shoes *> shoes_s;

void createItems()
{
    //Очистка
    {
        for (std::map<int, Clothes*>::iterator i = clothes_s.begin(),
             end = clothes_s.end(); i!= end; ++i)
        {
            delete i->second;
        }

        for (std::map<int, Shoes*>::iterator i = shoes_s.begin(),
             end = shoes_s.end(); i!= end; ++i)
        {
            delete i->second;
        }
    }
    //Одежда
    Sarafan *sarafan = new Sarafan("pic/clothes/female/sarafan/1.jpg");
    sarafan->setName(QT_TR_NOOP("Короткий сарафан"));
    sarafan->setId(1);
    clothes_s[1] = sarafan;

    //Обувь
    FlipFlop *flipFlop = new FlipFlop("pic/shoes/female/flipFlop/1.jpg");
    flipFlop->setName(QT_TR_NOOP("Простые шлёпанцы"));
    flipFlop->setId(1);
    shoes_s[1] = flipFlop;
}
