#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <QSettings>
#include <QCoreApplication>

#include "../Core/OldDate.h"
#include "Hero.h"
#include "Locations.h"

class Settings
{
public:
    Settings();

    static void save(const QString puth);
    static void load(const QString puth);
};

#endif // SETTINGS_H
