#ifndef CALCULATIONS_H
#define CALCULATIONS_H

#include <QObject>

#include "../Core/OldDate.h"
#include "Hero.h"

class Calculations : public QObject
{
    Q_OBJECT
public:
    explicit Calculations(OldDate *date, Hero *hero, QObject *parent = 0);

signals:

public slots:
    void ReduceTheNeed(long long houring);

private:
    OldDate *date;
    Hero *hero;

    long long houringToCalculation;

    long long houringToIncreaseFatigue, houringToIncreaseFatigueConst;

    const int houringToReduceTheNeed = 48;
};

#endif // CALCULATIONS_H
