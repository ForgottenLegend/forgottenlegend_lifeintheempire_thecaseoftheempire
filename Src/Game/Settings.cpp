#include "Settings.h"

Settings::Settings()
{
}

extern Hero *hero;
extern OldDate *date;

void Settings::save(const QString puth)
{
    if (hero && date)
    {
        QSettings settings(QCoreApplication::applicationDirPath () + puth + ".save", QSettings::IniFormat);
        settings.beginGroup("/Hero");
            settings.setValue("/id", hero->getId());
            settings.setValue("/name", hero->getName());
            settings.setValue("/level", hero->getLevel());
            settings.setValue("/exp", hero->getExp());
            settings.setValue("/maxCurrentExp", hero->getMaxCurrentExp());
            settings.setValue("/race", hero->getRace());
            settings.setValue("/strength", hero->getStrength());
            settings.setValue("/dexterity", hero->getDexterity());
            settings.setValue("/endurance", hero->getEndurance());
            settings.setValue("/reaction", hero->getReaction());
            settings.setValue("/intellect", hero->getIntellect());
            settings.setValue("/mind", hero->getMind());
            settings.setValue("/moral", hero->getMoral());
            settings.setValue("/penisSize", hero->getPenisSize());
            settings.setValue("/anusSize", hero->getAnusSize());
            settings.setValue("/vaginaSize", hero->getVaginaSize());
            settings.setValue("/gender", char(hero->getGender()));
            settings.setValue("/picPrefix", hero->getPicPrefix());
            settings.setValue("/health", hero->getHealth());
            settings.setValue("/age", hero->getAge());
            settings.setValue("/apparentAge", hero->getApparentAge());
            settings.setValue("/titsSize", hero->getTitsSize());
            settings.setValue("/dominate", hero->getDominate());
            settings.setValue("/will", hero->getWill());
            settings.setValue("/reputation", hero->getReputation());
            settings.setValue("/fame", hero->getFame());
            settings.setValue("/secondName", hero->getSecondName());
            settings.setValue("/lastName", hero->getLastName());
            settings.setValue("/shortName", hero->getShortName());
            settings.setValue("/capacityThroat", hero->getCapacityThroat());
            settings.setValue("/horny", hero->getHorhy());
            settings.setValue("/sexExp", hero->getSexExp());
            settings.setValue("/riches", hero->getRiches());
            settings.setValue("/magikExp", hero->getMagikExp());
            settings.setValue("/magikLevel", hero->getMagikLevel());
            settings.setValue("/mana", hero->getMana());
            settings.setValue("/wingsSize", hero->getWingsSize());
            settings.setValue("/wingsGeometry", hero->getWingsGeometry());
            settings.beginGroup("/money");
                settings.setValue("/rubl", hero->getMoney().getRubl());
                settings.setValue("/poltina", hero->getMoney().getPoltina());
                settings.setValue("/grivna", hero->getMoney().getGrivna());
                settings.setValue("/altyn", hero->getMoney().getAltyn());
                settings.setValue("/kopeyka", hero->getMoney().getKopeyka());
                settings.setValue("/denga", hero->getMoney().getDenga());
                settings.setValue("/polushka", hero->getMoney().getPolushka());
            settings.endGroup();
        settings.endGroup();
        settings.beginGroup("/Date");
            settings.setValue("/hour", date->getHour());
            settings.setValue("/houring", date->getHouring());
            settings.setValue("/dayOfTheWeek", date->getDayOfTheWeek());
            settings.setValue("/dayOfTheSorokovnik", date->getDayOfTheSorokovnik());
            settings.setValue("/sorokovnik", date->getSorokovnik());
            settings.setValue("/season", date->getSeason());
            settings.setValue("/year", date->getYear());
            settings.setValue("/dayOfStart", date->getDayOfTheStart());
        settings.endGroup();

        settings.beginGroup("/Place");

        settings.endGroup();
    }
}

void Settings::load(const QString puth)
{
    if (hero && date)
    {
        delete hero;
        delete date;
    }

    hero = new Hero();
    date = new OldDate();

    QSettings settings(QCoreApplication::applicationDirPath () + puth + ".save", QSettings::IniFormat);
    settings.beginGroup("/Hero");
        hero->setId(settings.value("/id", hero->getId()).toUInt());
        hero->setName(settings.value("/name", hero->getName()).toString());
        hero->setLevel(settings.value("/level", hero->getLevel()).toUInt());
        hero->setExp(settings.value("/exp", hero->getExp()).toULongLong());
        hero->setRace(settings.value("/race", hero->getRace()).toUInt());
        hero->setStrength(settings.value("/strength", hero->getStrength()).toUInt());
        hero->setDexterity(settings.value("/dexterity", hero->getDexterity()).toUInt());
        hero->setEndurance(settings.value("/endurance", hero->getEndurance()).toUInt());
        hero->setReaction(settings.value("/reaction", hero->getReaction()).toUInt());
        hero->setIntellect(settings.value("/intellect", hero->getIntellect()).toUInt());
        hero->setMind(settings.value("/mind", hero->getMind()).toUInt());
        hero->setMoral(settings.value("/moral", hero->getMoral()).toUInt());
        hero->setPenisSize(settings.value("/penisSize", hero->getPenisSize()).toUInt());
        hero->setAnusSize(settings.value("/anusSize", hero->getAnusSize()).toUInt());
        hero->setVaginaSize(settings.value("/vaginaSize", hero->getVaginaSize()).toInt());
        hero->setGender(Mob::Gender(settings.value("/gender", char(hero->getGender())).toInt()));
        hero->setPicPrefix(settings.value("/picPrefix", hero->getPicPrefix()).toString());
        hero->setHealth(settings.value("/health", hero->getHealth()).toLongLong());
        hero->setAge(settings.value("/age", hero->getAge()).toUInt());
        hero->setApparentAge(settings.value("/apparentAge", hero->getApparentAge()).toUInt());
        hero->setTitsSize(settings.value("/titsSize", hero->getTitsSize()).toUInt());
        hero->setDominate(settings.value("/dominate", hero->getDominate()).toLongLong());
        hero->setWill(settings.value("/will", hero->getWill()).toUInt());
        hero->setReputation(settings.value("/reputation", hero->getReputation()).toInt());
        hero->setFame(settings.value("/fame", hero->getFame()).toUInt());
        hero->setSecondName(settings.value("/secondName", hero->getSecondName()).toString());
        hero->setLastName(settings.value("/lastName", hero->getLastName()).toString());
        hero->setShortName(settings.value("/shortName", hero->getShortName()).toString());
        hero->setCapacityThroat(settings.value("/capacityThroat", hero->getCapacityThroat()).toUInt());
        hero->setHorny(settings.value("/horny", hero->getHorhy()).toUInt());
        hero->setSexExp(settings.value("/sexExp", hero->getSexExp()).toULongLong());
        hero->setRiches(settings.value("/riches", hero->getRiches()).toInt());
        hero->setMagikLevel(settings.value("/magikLevel", hero->getMagikLevel()).toUInt());
        hero->setMagikExp(settings.value("/magikExp", hero->getMagikExp()).toULongLong());
        hero->setMana(settings.value("/mana", hero->getMana()).toLongLong());
        hero->setWingsSize(settings.value("/wingsSize", hero->getWingsSize()).toUInt());
        hero->setWingsGeometry(settings.value("/wingsGeometry", hero->getWingsGeometry()).toUInt());
        settings.beginGroup("/money");
            long long rubl, poltina, grivna;
            int altyn, kopeyka, denga, polushka;
            rubl = settings.value("/rubl", hero->getMoney().getRubl()).toLongLong();
            poltina = settings.value("/poltina", hero->getMoney().getPoltina()).toLongLong();
            grivna = settings.value("/grivna", hero->getMoney().getGrivna()).toLongLong();
            altyn = settings.value("/altyn", hero->getMoney().getAltyn()).toInt();
            kopeyka = settings.value("/kopeyka", hero->getMoney().getKopeyka()).toInt();
            denga = settings.value("/denga", hero->getMoney().getDenga()).toInt();
            polushka = settings.value("/polushka", hero->getMoney().getPolushka()).toInt();
            hero->setMoney(Money(rubl, poltina, grivna, altyn, kopeyka, denga, polushka));
        settings.endGroup();
    settings.endGroup();
    settings.beginGroup("/Date");
        date->hour = settings.value("/hour", date->hour).toUInt();
        date->houring = settings.value("/houring", date->houring).toUInt();
        date->dayOfTheWeek = settings.value("/dayOfTheWeek", date->dayOfTheWeek).toUInt();
        date->dayOfTheSorokovnik = settings.value("/dayOfTheSorokovnik", date->dayOfTheSorokovnik).toUInt();
        date->sorokovnik = settings.value("/sorokovnik", date->sorokovnik).toUInt();
        date->season = settings.value("/season", date->season).toUInt();
        date->year = settings.value("/year", date->year).toLongLong();
        date->dayOfStart = settings.value("/dayOfStart", date->dayOfStart).toULongLong();
    settings.endGroup();

    settings.beginGroup("/Place");

    settings.endGroup();
}
