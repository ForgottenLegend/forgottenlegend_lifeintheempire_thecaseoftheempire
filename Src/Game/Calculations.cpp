#include "Calculations.h"

Calculations::Calculations(OldDate *date, Hero *hero, QObject *parent) :
    QObject(parent)
{
    this->date = date;
    this->hero = hero;

    connect(date, &OldDate::changeHouring, this, &Calculations::ReduceTheNeed);

    houringToCalculation = houringToReduceTheNeed;
}

void Calculations::ReduceTheNeed(long long houring)
{
    houringToCalculation -= houring;

    houringToIncreaseFatigueConst =
            ((date->getHourInTheDay() / (double)4) * date->getHouringInHour()) /
                        (double)hero->getMaxFatigue();
    houringToIncreaseFatigue = houringToIncreaseFatigueConst;
    while (houringToCalculation <= 0)
    {
        hero->addSatiety(-1);
        hero->addThirst(-1);

        if (!hero->isSleep())
            hero->addFatigue(-1);

        houringToCalculation += houringToReduceTheNeed;
    }
    if (hero->isSleep())
    {
        houringToIncreaseFatigue -= houring;
        while (houringToIncreaseFatigue <= 0)
        {
            hero->addFatigue(1);
            houringToIncreaseFatigue += houringToIncreaseFatigueConst;
        }
    }
}
