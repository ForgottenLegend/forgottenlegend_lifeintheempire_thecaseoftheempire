#include "Bedroom.h"

Bedroom::Bedroom(MainWindow *mw, Ui::MainWindow *ui, QObject *parent) :
    Room(mw, ui, parent)
{
    name = tr("Спальня");
    stage = 0;
}

void Bedroom::entry(Place *parent)
{
    connect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    connect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    connect(mw, SIGNAL(repaint()), this, SLOT(repaint()));

    if (parent)
        this->setParent(parent);

    text();
}

void Bedroom::quit()
{
    /*qDebug() << "Bedroom";
    qDebug() << disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    qDebug() << disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    qDebug() << disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));*/

    disconnect(ui->textBrowser,SIGNAL(anchorClicked(QUrl)),this,SLOT(anchorClicked(QUrl)));
    disconnect(ui->tableWidgetAction,SIGNAL(actionClicked(int)),this,SLOT(click(int)));
    disconnect(mw, SIGNAL(repaint()), this, SLOT(repaint()));
}

void Bedroom::anchorClicked(QUrl arg1)
{

}

void Bedroom::click(int index)
{
    switch (index)
    {
    case 0:
//        ++stage;
        repaint();
        break;
    case 1:
        quit();
        dynamic_cast<Room*>(parent())->entry();
        date->addHouring(2);

        break;
    case 2:
        quit();
        dynamic_cast<Room*>(parent()->children().at(1))->entry();
        date->addHouring(1);
        break;
    default:
        ui->textBrowser->append(
                    ConvertToHtml::getHeader(
                        ConvertToHtml::getFont(
                            ConvertToHtml::getColorStr(
                                tr("ДЕЙСТВИЕ ЕЩЁ НЕ ДОДЕЛАННО"),
                                ConvertToHtml::Color::Red),
                            (char)ConvertToHtml::Font::Bold)));
        ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
        break;
    }
}

void Bedroom::text()
{
    ui->textBrowser->clear();
    ui->tableWidgetAction->setRowCount(0);
    QString qs;

    qs.append(tr("%1%2").
              arg(ConvertToHtml::getHeader(name)).
              arg(Picture("pic/location/myHouse/rooms/bedroom.jpg").getStr()));
    switch (stage)
    {
    case 0:
        qs.append(ConvertToHtml::getParagraph(tr("В спальне стоит кровать и весят часы")));
        mw->addAction(tr("Пойти на кухню"), 2);
        mw->addAction (tr("Выйти в коридор"), 1);
        break;
    default:
        break;
    }

    ui->textBrowser->setHtml(qs);
}
