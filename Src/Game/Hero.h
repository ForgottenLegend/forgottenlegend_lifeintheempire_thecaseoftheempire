#ifndef HERO_H
#define HERO_H

#include <QObject>
#include <QDebug>

#include <vector>

#include "../Core/GameBase/HumanWingMob.h"
#include "../Core/GameBase/Money.h"
#include "../Core/GameBase/Clothes.h"

enum Cloth:char{nude = 0, lowerClothes = 1, clothes = 2, lowerClothes_clothes = 3, topClothes = 4, lowerClothes_topClothes = 5,
                clothes_topClothes = 6, lowerClothes_clothes_topClothes = 7};


class Hero : public QObject, public HumanWingMob
{
    Q_OBJECT
public:
    Hero(QObject *parent=0);
    Hero(const Hero *hero, QObject *parent=0);

    Money getMoney() const;
    void setMoney (const Money &money);
    void addMoney (const Money &money);

    //    void setLevel(unsigned int level);
    void addLevel(const long long &level);

    //    void setMagikLevel(unsigned int magikLevel);
    void addMagikLevel (const long long &magikLevel);

    void calculadeRich();

//    void setGender(Mob::Gender &gender);

    void setIntellect(const unsigned int &intellect);
    void addIntellect(const long long &intellect);

    void setMind(const unsigned int &mind);
    void addMind(const long long &mind);

    Cloth getClothes() const;

    int getSatiety() const;
    void setSatiety(const int &satiety);
    void addSatiety(const int &satiety);

    int getMaxSatiety() const;

    int getThirst() const;
    void setThirst(const int &thirst);
    void addThirst(const int &thirst);

    int getMaxThirst() const;

    bool isSleep() const;
    void setSleep(const bool &sleep);
signals:
    void changeLevel(signed char level);
    void changeMagikLevel(signed char magikLevel);

private:
    Money money;

    std::vector<Clothes *> clothes = {0, 0, 0};

    int satiety; //сытость
    int thirst; //жажда

    bool sleep;

    const int maxSatiety = 35, maxThirst = 35;

protected:
    void calculateMaxMana();

    void calculateMaxClearMind();
};

#endif // HERO_H
