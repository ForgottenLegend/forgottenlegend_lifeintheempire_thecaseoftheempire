#include "Hero.h"

Hero::Hero(QObject *parent) :
    QObject (parent),
    HumanWingMob()
{
    this->money     = 0;

    this->satiety   = maxSatiety;
    this->thirst    = maxThirst;

    this->sleep     = false;
}

Hero::Hero(const Hero *hero, QObject *parent) :
    QObject(parent),
    HumanWingMob(hero)
{
    this->money     = hero->money;
    this->satiety   = hero->satiety;
    this->thirst    = hero->thirst;
    this->sleep     = hero->sleep;
}

Money Hero::getMoney() const
{
    return money;
}

void Hero::setMoney(const Money &money)
{
    this->money = money;

    calculadeRich();
}

void Hero::addMoney(const Money &money)
{
    this->money += money;

    calculadeRich();
}

void Hero::addLevel(const long long &level)
{
    if (level >=0 || level <= this->level)
        this->level += level;
    else
        this->level = 0;
    if (level > 0)
        emit changeLevel(1);
    else
        if (level < 0)
            emit changeLevel(-1);
        else
            emit changeLevel(0);

    calculateMaxCurrentExp();

    calculateMaxWill();
}

void Hero::addMagikLevel(const long long &magikLevel)
{
    if (magikLevel >=0 || magikLevel <= this->magikLevel)
        this->magikLevel+=magikLevel;
    else
        this->magikLevel=1;
    if (magikLevel > 0)
        emit changeMagikLevel(1);
    else
        if (magikLevel < 0)
            emit changeMagikLevel(-1);
        else
            emit changeMagikLevel(0);

    calculateMaxCurrentMagikExp();

    calculateMaxWill();
}

void Hero::calculadeRich()
{
    if (money < Money(0))
        riches = -1;
    else
        if (money < Money(100))
            riches = 0;
        else
            if (money < Money(1000))
                riches = 1;
            else
                if (money < Money(10000))
                    riches = 2;
                else
                    if (money < Money(1000000))
                        riches = 3;
                    else
                        if (money < Money(1000000000))
                            riches = 4;
                        else
                            riches = 5;
}

void Hero::setIntellect(const unsigned int &intellect)
{
    Humanoid::setIntellect(intellect);
    calculateMaxMana();
}

void Hero::addIntellect(const long long &intellect)
{
    Humanoid::addIntellect(intellect);
    calculateMaxMana();
}

void Hero::setMind(const unsigned int &mind)
{
    Humanoid::setMind(mind);
    calculateMaxMana();
}

void Hero::addMind(const long long &mind)
{
    Humanoid::addMind(mind);
    calculateMaxMana();
}

Cloth Hero::getClothes() const
{
    if (clothes.at(0))
    {
        if (clothes.at(1))
        {
            if (clothes.at(2))
            {
                return Cloth::lowerClothes_clothes_topClothes;
            }
            return Cloth::lowerClothes_clothes;
        }
        else
            if (clothes.at(2))
            {
                return Cloth::lowerClothes_topClothes;
            }
    }
    else
    {
        if (clothes.at(1))
        {
            if (clothes.at(2))
            {
                return Cloth::clothes_topClothes;
            }
            return Cloth::clothes;
        }
        else
        {
            if (clothes.at(2))
            {
                return Cloth::topClothes;
            }
            return Cloth::nude;
        }
    }
    return Cloth::nude;
}

int Hero::getSatiety() const
{
    return satiety;
}

void Hero::setSatiety(const int &satiety)
{
    this->satiety = satiety;
    if (this->satiety > maxSatiety)
        this->satiety = maxSatiety;
}

void Hero::addSatiety(const int &satiety)
{
    this->satiety += satiety;
    if (this->satiety > maxSatiety)
        this->satiety = maxSatiety;

    if (this->satiety < 0)
    {
        this->addHealth(-5);
        this->satiety = this->maxSatiety / 5;
        if (this->satiety < 5)
            this->satiety = 5;
    }
}

int Hero::getMaxSatiety() const
{
    return maxSatiety;
}

int Hero::getThirst() const
{
    return thirst;
}

void Hero::setThirst(const int &thirst)
{
    this->thirst = thirst;
    if (this->thirst > maxThirst)
        this->thirst = maxThirst;
}

void Hero::addThirst(const int &thirst)
{
    this->thirst += thirst;
    if (this->thirst > maxThirst)
        this->thirst = maxThirst;

    if (this->thirst < 0)
    {
        this->addHealth(-10);
        this->thirst = this->maxThirst / 5;
        if (this->thirst < 5)
            this->thirst = 5;
    }
}

int Hero::getMaxThirst() const
{
    return maxThirst;
}

bool Hero::isSleep() const
{
    return sleep;
}

void Hero::setSleep(const bool &sleep)
{
    this->sleep = sleep;
}

void Hero::calculateMaxMana()
{
    SuperNaturalMob::calculateMaxMana();
    if (maxMana > 0)
        maxMana += intellect / 15 + mind / 50;
}

void Hero::calculateMaxClearMind()
{
    Human::calculateMaxClearMind();
    maxClearMind += magikLevel * 2;
}

//void Hero::setGender(Mob::Gender &gender)
//{
//    Humanoid::setGender(gender);
//}
