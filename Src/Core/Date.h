#ifndef DATE_H
#define DATE_H

#include <QObject>

class Date : public QObject
{
    Q_OBJECT
public:
    explicit Date(QObject *parent = 0);

    unsigned char getHour() const;
    void setHour(short h);
    void addHour(short h);

    unsigned char getMinute() const;
    void setMinute(short m);
    void addMinute(short m);

    unsigned char getSecond() const;
    void setSecond(short s);
    void addSecond(short s);

    unsigned char getDayOfTheWeek() const;
    void setDayOfTheWeek(short day);
    void addDayOfTheWeek(const short &day);

    unsigned char getDayOfTheMonth() const;
    void setDayOfTheMonth(short day);
    void addDayOfTheMonth(const short &day);

    unsigned char getMonth() const;
    void setMonth(short m);
    void addMonth(const short &m);

    unsigned char getSeason() const;
    void setSeason(short s);

//    unsigned short detDayOfTheYear();
//    void setDayOfTheYear(int day);

    long long getYear() const;
    void setYear(const long long &year);
    void addYear(const long long &year);

    unsigned long long getDayOfTheStart() const;
    void setDayOfTheStart(unsigned long long &day);
    void addDayOfTheStart(const long long &day);

    QString getNameMonth() const;

    QString getNameDayOfTheWeek() const;

    QString getNameSeason() const;
signals:
    void changeDayOfStart(signed char day);

    void changeHour(signed char hour);

    void changeMinute(signed char minute);

    void changeSecond(signed char second);

public slots:

private:
    unsigned char   hour,
                    minute,
                    second,
                    dayOfTheWeek,
                    dayOfTheMonth,
                    month,
                    season;
//    unsigned short dayOfTheYear;
    long long year;
    unsigned long long dayOfStart;


//    const int dayInTheYear = 365;

    const QString nameMonth[12] = {tr("Январь"), tr("Февраль"),tr("Март"),tr("Апрель"),tr("Май"),tr("Июнь"),tr("Июль"),
                                   tr("Август"),tr("Сентябрь"), tr("Октябрь"),tr("Ноябрь"),tr("Декабрь")};

    const QString nameDayOfTheWeek[7] = {tr("Понедельник"), tr("Вторник"), tr("Среда"), tr("Четверг"),
                                         tr("Пятница"), tr("Суббота"), tr("Воскресенье")};

    const QString nameSeason[4] = {tr("Весна"), tr("Лето"), tr("Осень"), tr("Зима")};

    const unsigned short secondOfTheDay = (unsigned short)86400;
};

#endif // DATE_H
