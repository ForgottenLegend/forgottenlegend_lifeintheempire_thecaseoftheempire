#include "HumanWingMob.h"

HumanWingMob::HumanWingMob() :
    Human(),
    WingMob(),
    SuperNaturalMob()
{
}

HumanWingMob::HumanWingMob(const HumanWingMob *humanWingMob) :
    Human(humanWingMob),
    WingMob(humanWingMob),
    SuperNaturalMob(humanWingMob)
{
}
