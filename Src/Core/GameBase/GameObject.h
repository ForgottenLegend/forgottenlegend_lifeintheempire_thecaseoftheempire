#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QString>

class GameObject
{
public:
    GameObject();
    GameObject(const GameObject *gameObject);

    unsigned int getId();
    void setId(unsigned int id);

    QString getName() const;
    void setName(const QString &name);

//    void virtual calculateID();

protected:
    unsigned int id;

    QString name;
};

#endif // GAMEOBJECT_H
