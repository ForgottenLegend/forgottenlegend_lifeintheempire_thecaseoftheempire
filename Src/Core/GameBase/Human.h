#ifndef HUMAN_H
#define HUMAN_H

#include "Humanoid.h"

class Human : public Humanoid
{
public:
    Human();
    Human(const Human *human);

    unsigned int getReputation () const;
    void setReputation (const int &reputation);
    void addReputation (const short &reputation);

    unsigned int getFame () const;
    void setFame (const unsigned int &fame);
    void addFame (const int &fame);

    QString getSecondName() const;
    void setSecondName (const QString &secondName);

    QString getLastName () const;
    void setLastName (const QString &lastName);

    QString getShortName () const;
    void setShortName (const QString &shortName);

    unsigned char getCapacityThroat() const;
    void setCapacityThroat(const unsigned char &capacity);
    void addCapacityThroat(const signed char &capacity);

    unsigned char getHorhy () const;
    void setHorny (const unsigned char &horny);
    void addHorny (const signed char &horny);

    unsigned long long getSexExp () const;
    void setSexExp (const unsigned long long &sexExp);
    void addSexExp (const long long &sexExp);

    signed char getRiches() const;
    void setRiches(const signed char &riches);

    virtual void setRace (const unsigned int &race);
    virtual void addRace (const long long &race);

    virtual void setStrength (const unsigned int &strength);
    virtual void addStrength (const long long &strength);

    virtual void setDexterity (const unsigned int &dexterity);
    virtual void addDexterity(const long long &dexterity);

    virtual void setEndurance (const unsigned int &endurance);
    virtual void addEndurance (const long long &endurance);

    virtual void setReaction (const unsigned int &reaction);
    virtual void addReaction (const long long &reaction);

    virtual void setIntellect (const unsigned int &intellect);
    virtual void addIntellect (const long long &intellect);

    virtual void setMind (const unsigned int &mind);
    virtual void addMind (const long long &mind);

    unsigned int getMaxWill() const;
protected:
    void calculateMaxWill();

    int reputation;
    unsigned int fame;//слава

    QString secondName,
            lastName,
            shortName;

    unsigned char   capacityThroat,
                    horny;

    unsigned long long sexExp;

    signed char riches;

    unsigned int maxWill;

    const unsigned int  maxRace = 500,
                        maxStrength = 1000,
                        maxDexterity = 1000,
                        maxEndurance = 1000,
                        maxReaction = 1000,
                        maxIntellect = 1000,
                        maxMind = 1000,
                        maxMoral = 1000;

    const unsigned int  maxReputation = 1000,
                        minReputation = -1000,
                        maxFame = 10000;

    const unsigned char maxCapacityThroat = 100,
                        maxHorny = 100;
};

#endif // HUMAN_H
