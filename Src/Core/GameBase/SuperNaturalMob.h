#ifndef SUPERNATURALMOB_H
#define SUPERNATURALMOB_H

class SuperNaturalMob
{
public:
    SuperNaturalMob();
    SuperNaturalMob(const SuperNaturalMob *superNaturalMob);

    unsigned int getMagikLevel() const;
    void setMagikLevel(const unsigned int &magikLevel);
    virtual void addMagikLevel (const long &magikLevel);

    unsigned long long getMagikExp() const;
    void setMagikExp(unsigned long long magikExp);
    void addMagikExp (long long &magikExp);

    void calculateMaxCurrentMagikExp();
    unsigned long long getMaxCurrentMagikExp() const;

    long long getMana() const;
    void setMana(const long long &mana);
    void addMana (const long long &mana);

    long long getMaxMana() const;
protected:
    unsigned int magikLevel;
    unsigned long long magikExp, maxCurrentMagikExp;

    long long mana, maxMana;

    virtual void calculateMaxMana();
};

#endif // SUPERNATURALMOB_H
