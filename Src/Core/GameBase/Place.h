#ifndef PLACE_H
#define PLACE_H

#include <QObject>
#include <QScrollBar>
#include <QRegExp>
#include <QMessageBox>

#include "GameObject.h"
#include "ui_MainWindow.h"
#include "../../Interface/MainWindow.h"
#include "../OldDate.h"
#include "../../Interface/AnimatedTextBrowser.h"

extern OldDate *date;

class MainWindow;

class Place : public QObject, public GameObject
{
    Q_OBJECT
public:
    explicit Place(MainWindow *mw, Ui::MainWindow *ui, QObject *parent=0);
    explicit Place(Place * place);

    virtual void entry(Place *parent = 0)=0;

    virtual void quit()=0;

signals:

public slots:
protected slots:
    virtual void anchorClicked(QUrl arg1)=0;

//    virtual void click(QModelIndex index)=0;
    virtual void click(int index) = 0;

    virtual void repaint();

protected:
    MainWindow *mw;
    Ui::MainWindow *ui;

    virtual void text() = 0;
};

#endif // PLACE_H
