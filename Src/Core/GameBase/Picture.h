#ifndef PICTURE_H
#define PICTURE_H

#include <QDebug>

#include <QPixmap>
#include "../../Interface/AnimatedTextBrowser.h"

extern AnimatedTextBrowser *textBrowser;

class Picture
{
public:
    Picture(QString imgSrc);

    void setPic(const QString imgSrc);

    int getHeight() const;
    int getWidth() const;

    QString getStr(const bool isDiv = true) const;

    void calculateSize();

private:

    QString src;

    int height;
    int width;

    double coef;
    double coefW;
};

#endif // PICTURE_H
