#ifndef TRANSPORT_H
#define TRANSPORT_H

#include "GameObject.h"

class Transport : public GameObject
{
public:
    Transport();

protected:
    unsigned int speed;

    unsigned int fuelConsumption; //Прожёрливость
};

#endif // TRANSPORT_H
