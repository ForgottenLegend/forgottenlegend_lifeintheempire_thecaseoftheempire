#ifndef HUMANWINGMOB_H
#define HUMANWINGMOB_H

#include "Human.h"
#include "WingMob.h"
#include "SuperNaturalMob.h"

class HumanWingMob : public Human, public WingMob, public SuperNaturalMob
{
public:
    HumanWingMob();
    HumanWingMob(const HumanWingMob *humanWingMob);
};

#endif // HUMANWINGMOB_H
