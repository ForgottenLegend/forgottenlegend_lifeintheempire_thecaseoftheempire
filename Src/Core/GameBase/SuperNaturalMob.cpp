#include "SuperNaturalMob.h"

SuperNaturalMob::SuperNaturalMob()
{
    this->magikExp              = 0;
    this->magikLevel            = 0;
    this->maxCurrentMagikExp    = 0;
    this->mana                  = 0;
    this->maxMana               = 0;
}

SuperNaturalMob::SuperNaturalMob(const SuperNaturalMob *superNaturalMob)
{
    this->magikExp              = superNaturalMob->magikExp;
    this->magikLevel            = superNaturalMob->magikLevel;
    this->maxCurrentMagikExp    = superNaturalMob->maxCurrentMagikExp;
    this->mana                  = superNaturalMob->mana;
    this->maxMana               = superNaturalMob->maxMana;
}

unsigned int SuperNaturalMob::getMagikLevel() const
{
    return magikLevel;
}

void SuperNaturalMob::setMagikLevel(const unsigned int &magikLevel)
{
    this->magikLevel = magikLevel;
    calculateMaxCurrentMagikExp();
    calculateMaxMana();
}

void SuperNaturalMob::addMagikLevel(const long &magikLevel)
{
    if (magikLevel >=0 || -magikLevel <= this->magikLevel)
        this->magikLevel += magikLevel;
    else
        this->magikLevel = 1;
    calculateMaxCurrentMagikExp();
    calculateMaxMana();
}

unsigned long long SuperNaturalMob::getMagikExp() const
{
    return magikExp;
}

void SuperNaturalMob::setMagikExp(unsigned long long magikExp)
{
    while (magikExp >= maxCurrentMagikExp)
    {
        addMagikLevel(1);
        magikExp-=maxCurrentMagikExp;
    }
    this->magikExp = magikExp;
}

void SuperNaturalMob::addMagikExp(long long &magikExp)
{
    long currentMagikExp = this->magikExp + magikExp;
    while (currentMagikExp >= maxCurrentMagikExp)
    {
        addMagikLevel(1);
        currentMagikExp -= maxCurrentMagikExp;
    }
    if (currentMagikExp < 0)
        currentMagikExp = 0;
    this->magikExp = currentMagikExp;
}

void SuperNaturalMob::calculateMaxCurrentMagikExp()
{
    maxCurrentMagikExp = (magikLevel + 1) * 100;
}

unsigned long long SuperNaturalMob::getMaxCurrentMagikExp() const
{
    return maxCurrentMagikExp;
}

long long SuperNaturalMob::getMana() const
{
    return mana;
}

void SuperNaturalMob::setMana(const long long &mana)
{
    if (mana > maxMana)
        this->mana = maxMana;
    else
        this->mana = mana;
}

void SuperNaturalMob::addMana(const long long &mana)
{
    this->mana += mana;
    if (this->mana < 0)
        this->mana = 0;
    if (this->mana > maxMana)
        this->mana = maxMana;
}

void SuperNaturalMob::calculateMaxMana()
{
    maxMana = magikLevel * 10;
}

long long SuperNaturalMob::getMaxMana() const
{
    return maxMana;
}
