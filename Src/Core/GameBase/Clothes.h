#ifndef CLOTHES_H
#define CLOTHES_H

#include "QString"

#include "Picture.h"
#include "Item.h"

class Clothes : public Item
{
public:
    Clothes(QString picFront, bool part = true, QString picBack = "");
    Clothes(const Clothes *clothes);

    QString getStr();
protected:
    QString picFront,
            picBack;
    bool part;
};

#endif // CLOTHES_H
