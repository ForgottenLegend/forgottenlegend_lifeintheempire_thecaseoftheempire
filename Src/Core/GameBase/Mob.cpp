#include "Mob.h"

Mob::Mob() :
    GameObject()
{
    this->level         = 0;
    this->race          = 0;
    this->strength      = 0;
    this->dexterity     = 0;
    this->endurance     = 0;
    this->reaction      = 0;
    this->intellect     = 0;
    this->mind          = 0;
    this->moral         = 0;
    this->penisSize     = 0;
    this->anusSize      = 0;
    this->vaginaSize    = -1;
    this->gender        = Gender::Asexual;
    this->picPrefix     = "";

    calculateMaxCurrentExp();

    this->exp           = 0;

    calculateMaxHealth();

    this->health        = this->maxHealth;

    calculateMaxFatigue();
    this->fatigue       = this->maxFatigue;

    calculateMaxClearMind();
    this->clearMind     = this->maxClearMind;
}

Mob::Mob(const Mob *mob) :
    GameObject(mob)
{
    this->level         = mob->level;
    this->exp           = mob->exp;
    this->maxCurrentExp = mob->maxCurrentExp;
    this->race          = mob->race;
    this->strength      = mob->strength;
    this->dexterity     = mob->dexterity;
    this->endurance     = mob->endurance;
    this->reaction      = mob->reaction;
    this->intellect     = mob->intellect;
    this->mind          = mob->mind;
    this->moral         = mob->moral;
    this->penisSize     = mob->penisSize;
    this->anusSize      = mob->anusSize;
    this->vaginaSize    = mob->vaginaSize;
    this->gender        = mob->gender;
    this->picPrefix     = mob->picPrefix;
    this->health        = mob->health;
    this->maxHealth     = mob->maxHealth;
    this->fatigue       = mob->fatigue;
    this->maxFatigue    = mob->maxFatigue;
    this->clearMind     = mob->clearMind;
}

unsigned int Mob::getLevel() const
{
    return level;
}

void Mob::setLevel(const unsigned int &level)
{
    this->level = level;

    calculateMaxCurrentExp();
    calculateMaxHealth();
}

void Mob::addLevel(const long long &level)
{
    if (level >=0 || -level <= this->level)
        this->level += level;
    else
        this->level=0;

    calculateMaxCurrentExp();
    calculateMaxHealth();
}

unsigned int Mob::getExp() const
{
    return exp;
}

void Mob::setExp(unsigned long long exp)
{
    while (exp >= maxCurrentExp)
    {
        exp -= maxCurrentExp;
        addLevel(1);
    }
    this->exp = exp;
}

void Mob::addExp(long long &exp)
{
    long long currentExp = this->exp + exp;
    while (currentExp >= maxCurrentExp)
    {
        currentExp -= maxCurrentExp;
        addLevel(1);
    }
    while (currentExp < 0)
    {
        addLevel(-1);
        currentExp += maxCurrentExp;
    }
    this->exp = currentExp;
}



void Mob::calculateMaxCurrentExp()
{
    maxCurrentExp = (level + 1)*100;
}

unsigned long long Mob::getMaxCurrentExp() const
{
    return maxCurrentExp;
}

unsigned int Mob::getRace() const
{
    return race;
}

void Mob::setRace(const unsigned int &race)
{
    this->race = race;
}

void Mob::addRace(const long long &race)
{
    if (race >=0 || -race < this->race)
        this->race += race;
    else
        this->race = 1;
}

unsigned int Mob::getStrength() const
{
    return strength;
}

void Mob::setStrength(const unsigned int &strength)
{
    this->strength = strength;

    calculateMaxHealth();
    calculateMaxFatigue();
}

void Mob::addStrength(const long long &strength)
{
    if (strength >=0 || -strength < this->strength)
        this->strength += strength;
    else
        this->strength = 1;

    calculateMaxHealth();
    calculateMaxFatigue();
}

unsigned int Mob::getDexterity() const
{
    return dexterity;
}

void Mob::setDexterity(const unsigned int &dexterity)
{
    this->dexterity = dexterity;
}

void Mob::addDexterity(const long long &dexterity)
{
    if (dexterity >=0 || -dexterity < this->dexterity)
        this->dexterity += dexterity;
    else
        this->dexterity = 1;
}

unsigned int Mob::getEndurance() const
{
    return endurance;
}

void Mob::setEndurance(const unsigned int &endurance)
{
    this->endurance = endurance;

    calculateMaxHealth();
    calculateMaxFatigue();
}

void Mob::addEndurance(const long long &endurance)
{
    if (endurance >=0 || -endurance < this->endurance)
        this->endurance += endurance;
    else
        this->endurance = 1;

    calculateMaxHealth();
    calculateMaxFatigue();
}

unsigned int Mob::getReaction() const
{
    return reaction;
}

void Mob::setReaction(const unsigned int &reaction)
{
    this->reaction = reaction;

    calculateMaxHealth();
}

void Mob::addReaction(const long long &reaction)
{
    if (reaction >=0 || -reaction < this->reaction)
        this->reaction += reaction;
    else
        this->reaction = 1;

    calculateMaxHealth();
}

unsigned int Mob::getIntellect() const
{
    return intellect;
}

void Mob::setIntellect(const unsigned int &intellect)
{
    this->intellect = intellect;

    calculateMaxClearMind();
}

void Mob::addIntellect(const long long &intellect)
{
    if (intellect >=0 || -intellect < this->intellect)
        this->intellect += intellect;
    else
        this->intellect = 1;

    calculateMaxClearMind();
}

unsigned int Mob::getMind() const
{
    return mind;
}

void Mob::setMind(const unsigned int &mind)
{
    this->mind = mind;

    calculateMaxClearMind();
}

void Mob::addMind(const long long &mind)
{
    if (mind >=0 || -mind < this->mind)
        this->mind += mind;
    else
        this->mind = 1;

    calculateMaxClearMind();
}

unsigned int Mob::getMoral() const
{
    return moral;
}

void Mob::setMoral(const unsigned int &moral)
{
    if (moral > maxMoral)
        this->moral = maxMoral;
    else
        this->moral = moral;
}

void Mob::addMoral(const int &moral)
{
    if (moral >=0 || -moral < this->moral)
        this->moral += moral;
    else
        this->moral = 0;
}

unsigned char Mob::getPenisSize() const
{
    return penisSize;
}

void Mob::setPenisSize(const short &size)
{
    if (size > maxPenisSize && size >= 0)
        penisSize = maxPenisSize;
    else
        if (size < 0)
            penisSize = 0;
        else
            penisSize = size;
}

void Mob::addPenisSize(const short &size)
{
    if (size >= 0)
        if (size + penisSize >=maxPenisSize && size >= 0)
            penisSize = maxPenisSize;
        else
            penisSize += size;
    else
        if (size + penisSize >= 0)
            penisSize += size;
        else
            penisSize = 0;
}

unsigned char Mob::getAnusSize() const
{
    return anusSize;
}

void Mob::setAnusSize(const short &size)
{
    if (size > maxAnusSize && size >= 0)
        anusSize = maxAnusSize;
    else
        if (size < 1)
            anusSize = 1;
        else
            anusSize = size;
}

void Mob::addAnusSize(const short &size)
{
    if (size >= 0)
        if (size+anusSize >= maxAnusSize && size >= 0)
            anusSize = maxAnusSize;
        else
            anusSize+=size;
    else
        if (size + anusSize > 0)
            anusSize += size;
        else
            if (anusSize > 0)
                anusSize = 1;
}

signed char Mob::getVaginaSize() const
{
    return vaginaSize;
}

void Mob::setVaginaSize(const short &size)
{
    if (size > maxVaginaSize && size >= 0)
        vaginaSize = maxVaginaSize;
    else
        if (size < -1)
            vaginaSize = -1;
        else
            vaginaSize = size;
}

void Mob::addVaginaSize(const short &size)
{
    if (size >= 0)
        if (size + vaginaSize >= maxVaginaSize && size >= 0)
            vaginaSize = maxVaginaSize;
        else
            vaginaSize += size;
    else
        if (size + vaginaSize >0)
            vaginaSize += size;
        else
            if (vaginaSize > 0)
                vaginaSize = 1;
}

Mob::Gender Mob::getGender() const
{
    return gender;
}

void Mob::setGender(Gender gender)
{
    if (int(gender) > 3)
        this->gender = Gender::Herm;
    else
        if (int(gender) < 0)
            this->gender = Gender::Asexual;
        else
            this->gender = gender;
    if (this->gender == Gender::Asexual)
    {
        penisSize = 0;
        vaginaSize = -1;
    }
    else
        if (this->gender == Gender::Male)
        {
            if (penisSize == 0)
                penisSize = rand() % maxPenisSize + 1;
            vaginaSize = -1;
        }
        else
            if (this->gender == Gender::Female)
            {
                penisSize = 0;
                if (vaginaSize == -1)
                    vaginaSize = 0;
            }
            else
                if (this->gender == Gender::Herm)
                {
                    if (penisSize == 0)
                        penisSize = rand() % maxPenisSize + 1;
                    if (vaginaSize == -1)
                        vaginaSize = 0;
                }
}

QString Mob::getNameOfTheGender() const
{
    return nameOfTheGender[int(gender)];
}

QString Mob::getPicPrefix() const
{
    return picPrefix;
}

void Mob::setPicPrefix(const QString &picPrefix)
{
    if (!picPrefix.isNull() && !picPrefix.isEmpty())
        this->picPrefix = picPrefix;
}

long long Mob::getHealth() const
{
    return health;
}

void Mob::setHealth(const long long &health)
{
    if (health > maxHealth && this->health >= 0)
        this->health = maxHealth;
    else
        this->health = health;
}

void Mob::addHealth(const long long &health)
{
    this->health += health;
    if (this->health > maxHealth && this->health >= 0)
        this->health = maxHealth;
    if (this->health < 0)
    {
        this->health = 1;
        QMessageBox::information(0, QT_TR_NOOP("Заглушка"), QT_TR_NOOP("Вы, типо, умерли"));
    }
}


long long Mob::getMaxHealth() const
{
    return maxHealth;
}

long long Mob::getFatigue() const
{
    return fatigue;
}

void Mob::setFatigue(const long long &fatigue)
{
    this->fatigue = fatigue;
    if (this->fatigue > this->maxFatigue && this->fatigue >= 0)
        this->fatigue = this->maxFatigue;
}

void Mob::addFatigue(const long long &fatigue)
{
    this->fatigue += fatigue;
    if (this->fatigue > this->maxFatigue && this->fatigue >= 0)
        this->fatigue = this->maxFatigue;
}

unsigned long long Mob::getMaxFatigue() const
{
    return maxFatigue;
}

long long Mob::getClearMind() const
{
    return clearMind;
}

void Mob::setClearMind(const long long &clearMind)
{
    this->clearMind = clearMind;
    if (this->clearMind > maxClearMind && this->clearMind >= 0)
        this->clearMind = maxClearMind;
}

void Mob::addClearMind(const long long &clearMind)
{
    this->clearMind += clearMind;
    if (this->clearMind > maxClearMind && this->clearMind >= 0)
        this->clearMind = maxClearMind;
}

unsigned long long Mob::getMaxClearMind() const
{
    return maxClearMind;
}

void Mob::calculateMaxHealth()
{
    maxHealth = level * 5 + 20 + endurance / 15 + strength / 50 + reaction / 250;
}

void Mob::calculateMaxFatigue()
{
    maxFatigue = level * 2 + 10 + endurance / 15 + strength / 50;
}

void Mob::calculateMaxClearMind()
{
    maxClearMind = level * 2 + 10 + mind / 15 + intellect / 50;
}
