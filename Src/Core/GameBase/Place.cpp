#include "Place.h"

Place::Place(MainWindow *mw, Ui::MainWindow *ui, QObject *parent) :
    QObject(parent)
{
    this->mw=mw;
    this->ui=ui;
}

Place::Place(Place *place) :
    QObject(place)
{
    this->mw = place->mw;
    this->ui = place->ui;
}

void Place::repaint()
{
    QString text = ui->textBrowser->toHtml();
    QRegExp qre("<img src=\"([/.:\\w\\d\\s]+)\" width=\"[/.:\\w\\d\\s]+\" height=\"[/.:\\w\\d\\s]+\" />");
    /*qDebug() << qre.indexIn(ui->textBrowser->toHtml());*/
    int pos = 0;
    while (pos >= 0)
    {
        pos = qre.indexIn(text, pos);
        if (pos >=0)
        {
            text.replace(qre.cap(), Picture(qre.cap(1)).getStr());
            pos += qre.cap().length();
        }
    }
    ui->textBrowser->setHtml(text);
}
