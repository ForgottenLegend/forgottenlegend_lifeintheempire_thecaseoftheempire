#ifndef WINGMOB_H
#define WINGMOB_H

#include <QString>

class WingMob
{
public:
    WingMob();
    WingMob(const WingMob *wingMob);

    unsigned short getWingsSize() const;
    void setWingsSize (const unsigned short &wingsSize);
    virtual void addSizeWings (const short &wingsSize);

    unsigned char getWingsGeometry() const;
    void setWingsGeometry (const unsigned char &wingsGeometry);
    QString getNameGeometryWings () const;

protected:
    unsigned short wingsSize;
    unsigned char wingsGeometry;

private:
    const unsigned char maxWingsGeometry = 2;

    QString nameWingsGeometry[3] = {QT_TR_NOOP("боевые"),QT_TR_NOOP("скоростные"),QT_TR_NOOP("грузовые")};
};

#endif // WINGMOB_H
