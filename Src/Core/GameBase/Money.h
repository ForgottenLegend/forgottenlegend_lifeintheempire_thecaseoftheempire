#ifndef MONEY_H
#define MONEY_H

//#include "Settings.h"

class Money
{
public:
    Money();
    Money(long long rubl, long long poltina, long long grivna = 0, int altyn = 0, int kopeyka = 0, short denga = 0,
          short polushka = 0);
    Money(const Money &money);
    Money(long long kopeyka, long long denga = 0, long long polushka = 0);

    long long getRubl() const;
    long long getPoltina() const;
    long long getGrivna() const;

    int getAltyn() const;
    int getKopeyka() const;

    short getDenga() const;
    short getPolushka() const;

    Money &operator=(const Money &money);
    Money operator+(const Money &right);
    Money operator-(const Money &right);
    Money &operator+=(const Money &right);
    Money &operator-=(const Money &right);
    bool operator==(const Money &right);
    bool operator!=(const Money &right);
    bool operator<=(const Money &right);
    bool operator>=(const Money &right);
    bool operator<(const Money &right);
    bool operator>(const Money &right);

private:
    long long   rubl,       //рубль
                poltina,    //полтина
                grivna;     //гривна
    int         altyn,      //алтын
                kopeyka;    //копейка
    short       denga,      //деньга
                polushka;   //полтушка
};

#endif // MONEY_H
