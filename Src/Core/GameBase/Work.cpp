#include "Work.h"

Work::Work()
{
}

unsigned int Work::getLabourWorkExperience() const
{
    return labourWorkExperience;
}

void Work::setLabourWorkExperience(const unsigned int &labourWorkExperience)
{
    this->labourWorkExperience = labourWorkExperience;
}

void Work::addLabourWorkExperience(const int &labourWorkExperience)
{
    if (labourWorkExperience > 0 || -labourWorkExperience < this->labourWorkExperience)
        this->labourWorkExperience = labourWorkExperience;
    else
        this->labourWorkExperience = 0;
}
