#ifndef SHOES_H
#define SHOES_H

#include "QString"

#include "Picture.h"
#include "Item.h"

class Shoes : public Item
{
public:
    Shoes(QString pic);
    Shoes(const Shoes *shoes);

    QString getStr();
private:
    QString pic;
};
#endif // SHOES_H
