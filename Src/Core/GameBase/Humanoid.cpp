#include "Humanoid.h"

Humanoid::Humanoid() :
    Mob()
{
    this->age           = 0;
    this->apparentAge   = 0;
    this->titsSize      = 0;
    this->dominate      = 0;
    this->will          = 0;
}

Humanoid::Humanoid(const Humanoid *humanoid) :
    Mob(humanoid)
{
    this->age           = humanoid->age;
    this->apparentAge   = humanoid->apparentAge;
    this->titsSize      = humanoid->titsSize;
    this->dominate      = humanoid->dominate;
    this->will          = humanoid->will;
}

unsigned int Humanoid::getAge() const
{
    return age;
}

void Humanoid::setAge(const unsigned int &age)
{
    this->age = age;
}

void Humanoid::addAge(const int &age)
{
    if (age > 0 || -age < this->age)
        this->age += age;
    else
        this->age = 0;
}

unsigned int Humanoid::getApparentAge() const
{
    return apparentAge;
}

void Humanoid::setApparentAge(const unsigned int &apparentAge)
{
    this->apparentAge = apparentAge;
}

void Humanoid::addApparentAge(const int &apparentAge)
{
    if (apparentAge > 0 || -apparentAge < this->apparentAge)
        this->apparentAge += apparentAge;
    else
        this->apparentAge = 0;
}

unsigned char Humanoid::getTitsSize() const
{
    return titsSize;
}

void Humanoid::setTitsSize(const unsigned char &titsSize)
{
    if (titsSize > maxTitsSize)
        this->titsSize = maxTitsSize;
    else
        this->titsSize = titsSize;
}

void Humanoid::addTitsSIze(const short &titsSize)
{
    if (titsSize >= 0 || -titsSize <= this->titsSize)
        this->titsSize += titsSize;
    else
        this->titsSize = 0;

    if (this->titsSize > maxTitsSize)
        this->titsSize = maxTitsSize;
}

QString Humanoid::getNameTitsSize() const
{
    return nameTitsSize[titsSize];
}

void Humanoid::setGender(Gender gender)
{
    if (int(gender) > 3)
        this->gender = Gender::Herm;
    else
        if (int(gender) < 0)
            this->gender = Gender::Asexual;
        else
            this->gender = gender;
    if (this->gender == Gender::Asexual)
    {
        penisSize = 0;
        vaginaSize = -1;
        titsSize = 0;
    }
    else
        if (this->gender == Gender::Male)
        {
            if (penisSize == 0)
                penisSize = rand() % maxPenisSize + 1;
            vaginaSize = -1;
            titsSize = 0;
        }
        else
            if (this->gender == Gender::Female)
            {
                penisSize = 0;
                if (vaginaSize == -1)
                    vaginaSize = 0;
                if (titsSize == 0)
                    titsSize = rand() % (maxTitsSize + 1);
            }
            else
                if (this->gender == Gender::Asexual)
                {
                    if (penisSize == 0)
                        penisSize = rand() % maxPenisSize + 1;
                    if (vaginaSize == -1)
                        vaginaSize = 0;
                    if (titsSize == 0)
                        titsSize = rand() % (maxTitsSize + 1);
                }
}

void Humanoid::setGender(Gender gender, const unsigned char titsSize)
{
    if (titsSize)
    {
        Mob::setGender(gender);
        this->titsSize = titsSize > maxTitsSize ? maxTitsSize : titsSize;
    }
}

long long  Humanoid::getDominate() const
{
    return dominate;
}

void Humanoid::setDominate(const long long &dominate)
{
    if (dominate > maxDominate)
        this->dominate = maxDominate;
    else
        if (dominate < minDominate)
            this->dominate = minDominate;
        else
            this->dominate = dominate;
}

void Humanoid::addDominate(const long long &dominate)
{
    if (dominate > 0)
        if (this->dominate + dominate > maxDominate)
            this->dominate = maxDominate;
        else
            this->dominate += dominate;
    else
        if (this->dominate + dominate < minDominate)
            this->dominate = minDominate;
        else
            this->dominate += dominate;
}

unsigned int Humanoid::getWill() const
{
    return will;
}

void Humanoid::setWill(const unsigned int &will)
{
    if (will > maxWill)
        this->will = maxWill;
    else
        this->will = will;
    calculateMaxClearMind();
}

void Humanoid::addWill(const long long &will)
{
    if (will >=0 || -will < this->will)
        if (will > maxWill)
            this->will = maxWill;
        else
            this->will = will;
    else
        this->will = 0;
    calculateMaxClearMind();
}

void Humanoid::calculateMaxClearMind()
{
    Mob::calculateMaxClearMind();
    maxClearMind += maxWill / 25;
}
