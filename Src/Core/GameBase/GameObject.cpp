#include "GameObject.h"

GameObject::GameObject()
{
    id = 0;
    name = "";
}

GameObject::GameObject(const GameObject *gameObject)
{
    this->id    = gameObject->id;
    this->name  = gameObject->name;
}

unsigned int GameObject::getId()
{
    return id;
}

void GameObject::setId(unsigned int id)
{
    this->id = id;
}

QString GameObject::getName() const
{
    return name;
}

void GameObject::setName(const QString &name)
{
    this->name = name;
}

/*void GameObject::calculateID()
{
    if (objects.size() == 0)
        id= 0;
    else
    {
        id= objects.back()->id+1;
    }
}*/
