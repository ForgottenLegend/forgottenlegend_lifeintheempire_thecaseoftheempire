#include "ConvertToHtml.h"

ConvertToHtml::ConvertToHtml()
{
}

QString ConvertToHtml::getHeader(const QString &str, const bool isDiv)
{
    if (isDiv)
        return QString("<div align=\"center\"><b>%1</b></div>").arg(str);
    else
        return QString("<b>%1</b>").arg(str);
}

QString ConvertToHtml::getFont(const QString &str, const char &font)
{
    QString buf = str;
    if (font & static_cast<char>(Font::Italic))
        buf = QString("<i>%1</i>").arg(buf);
    if (font & static_cast<char>(Font::Bold))
        buf = QString("<b>%1</b>").arg(buf);
    if (font & static_cast<char>(Font::Underline))
        buf = QString("<u>%1</u>").arg(buf);
    return buf;
}

QString ConvertToHtml::getParagraph(const QString &str, const char &margin)
{
    if ((margin & char(Margin::Bottom)) && (margin & char(Margin::Left)) && (margin & char(Margin::Right)) &&
            (margin & char(Margin::Top)))
        return QString("<div align=\"left\" style=\"text-indent:%1px;margin:-%1px\">%2</div>").arg(textBrowser->font().pointSize()*2).arg(str);
    if ((margin & char(Margin::Bottom)) && (margin & char(Margin::Top)))
        return QString("<div align=\"left\" style=\"text-indent:%1px;margin-top:-%1px;margin-bottom:-%1px\">%2</div>").arg(textBrowser->font().
                                                                                                        pointSize()*2).arg(str);
    if (margin & char(Margin::Bottom))
        return QString("<div align=\"left\" style=\"text-indent:%1px;margin-bottom:-%1px;margin-top:%1\">%2</div>").arg(textBrowser->font().
                                                                                       pointSize()*2).arg(str);
    if (margin & char(Margin::Top))
        return QString("<div align=\"left\" style=\"text-indent:%1px;margin-top:-%1px;margin-bottom:%1\">%2</div>").arg(textBrowser->font().
                                                                                    pointSize()*2).arg(str);
    return QString("<div align=\"left\">%1</div>").arg(str);
}

QString ConvertToHtml::getHtmlLocation(const QString &locanion, const QString id)
{
    return QString("<a href=\"%1\">%2</a>").arg(id, locanion);
}

QString ConvertToHtml::getLink(const QString &str, const QString &action)
{
    return QString("<a href=\"%1\">%2</a>").arg(action, str);
}

QString ConvertToHtml::getColorStr(const QString &str, const Color &color)
{
    switch (color) {
    case Color::Red:
        return QString("<span style=\"color:red\">%1</span>").arg(str);
        break;
    case Color::Green:
        return QString("<span style=\"color:green\">%1</span>").arg(str);
        break;
    case Color::Blue:
        return QString("<span style=\"color:blue\">%1</span>").arg(str);
        break;
    case Color::Purple:
        return QString("<span style=\"color:purple\">%1</span>").arg(str);
        break;
    case Color::Black:
        return QString("<span style=\"color:black\">%1</span>").arg(str);
        break;
    default:
        break;
    }
    return str;
}
