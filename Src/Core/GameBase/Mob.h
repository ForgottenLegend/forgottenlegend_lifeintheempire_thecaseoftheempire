#ifndef MOB_H
#define MOB_H

#include <QMessageBox>

#include "GameObject.h"

class Mob : public GameObject
{
public:
    enum class Gender {Asexual = 0, Male = 1, Female = 2, Herm = 3};

    Mob();
    Mob(const Mob *mob);

    unsigned int getLevel() const;
    void setLevel(const unsigned int &level);
    virtual void addLevel(const long long &level);

    unsigned int getExp() const;
    void setExp(unsigned long long exp);
    void addExp(long long &exp);

    unsigned long long getMaxCurrentExp() const;

    unsigned int getRace() const;
    virtual void setRace (const unsigned int &race);
    virtual void addRace (const long long &race);

    unsigned int getStrength() const;
    virtual void setStrength (const unsigned int &strength);
    virtual void addStrength (const long long &strength);

    unsigned int getDexterity() const;
    virtual void setDexterity (const unsigned int &dexterity);
    virtual void addDexterity(const long long &dexterity);

    unsigned int getEndurance() const;
    virtual void setEndurance (const unsigned int &endurance);
    virtual void addEndurance (const long long &endurance);

    unsigned int getReaction() const;
    virtual void setReaction (const unsigned int &reaction);
    virtual void addReaction (const long long &reaction);

    unsigned int getIntellect() const;
    virtual void setIntellect (const unsigned int &intellect);
    virtual void addIntellect (const long long &intellect);

    unsigned int getMind() const;
    virtual void setMind (const unsigned int &mind);
    virtual void addMind (const long long &mind);

    unsigned int getMoral() const;
    void setMoral (const unsigned int &moral);
    void addMoral (const int &moral);

    unsigned char getPenisSize() const;
    void setPenisSize(const short &size);
    void addPenisSize(const short &size);

    unsigned char getAnusSize() const;
    void setAnusSize(const short &size);
    void addAnusSize(const short &size);

    signed char getVaginaSize() const;
    void setVaginaSize(const short &size);
    void addVaginaSize(const short &size);

    Gender getGender() const;
    virtual void setGender(Gender gender);
    QString getNameOfTheGender() const;

    QString getPicPrefix () const;
    void setPicPrefix (const QString &picPrefix);

    long long getHealth() const;
    void setHealth(const long long &health);
    void addHealth(const long long &health);

    long long getMaxHealth() const;

    long long getFatigue() const;
    void setFatigue(const long long &fatigue);
    void addFatigue(const long long &fatigue);

    unsigned long long getMaxFatigue() const;

    long long getClearMind() const;
    void setClearMind(const long long &clearMind);
    void addClearMind(const long long &clearMind);

    unsigned long long getMaxClearMind() const;
protected:
    unsigned int level;
    unsigned long long exp, maxCurrentExp;

    unsigned int race;
    /*бег*/

    unsigned int strength, dexterity, endurance, reaction, intellect, mind, moral;
                /*сила, ловкость, выносливость, реакция, интеллект, разум, мораль*/

    unsigned char penisSize, anusSize;
    signed char vaginaSize;

    Gender gender;

    QString picPrefix;

    long long health, maxHealth;

    long long fatigue; //усталость
    unsigned long long maxFatigue;

    long long clearMind;
    unsigned long long maxClearMind;

    const unsigned int maxMoral = 1000;

    const unsigned char maxPenisSize = 30,
                        maxVaginaSize = 100,
                        maxAnusSize = 100;

    const QString nameOfTheGender[4] = {QT_TR_NOOP("беcполый"), QT_TR_NOOP("мужчина"),
                                        QT_TR_NOOP("женьщина"), QT_TR_NOOP("гемофродит")};

    void calculateMaxCurrentExp();
    void calculateMaxHealth();
    void calculateMaxFatigue();
    virtual void calculateMaxClearMind();
};

#endif // MOB_H
