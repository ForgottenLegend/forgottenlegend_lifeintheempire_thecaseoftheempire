#include "Money.h"

template <typename T>
T abs (T value)
{
    if (value < 0)
        value *= -1;
    return value;
}

Money::Money()
{
}

Money::Money(long long rubl, long long poltina, long long grivna, int altyn, int kopeyka, short denga, short polushka)
{
    while (abs(polushka) >= 2)
    {
        if (polushka < 0)
        {
            --denga;
            polushka += 2;
        }
        else
        {
            ++denga;
            polushka -= 2;
        }
    }

    while (abs(denga) >= 2)
    {
        if (denga < 0)
        {
            --kopeyka;
            denga += 2;
        }
        else
        {
            ++kopeyka;
            denga -= 2;
        }
    }

    while (abs(kopeyka) >= 3)
    {
        if (kopeyka < 0)
        {
            --altyn;
            kopeyka += 3;
        }
        else
        {
            ++altyn;
            kopeyka -= 3;
        }
    }

    while (abs(altyn) >= 3 && abs(kopeyka) >= 1)
    {
        if (altyn < 0)
        {
            --grivna;
            ++kopeyka;
            altyn += 3;
        }
        else
        {
            ++grivna;
            --kopeyka;
            altyn -= 3;
        }
    }

    while (abs(grivna) >= 5)
    {
        if (grivna < 0)
        {
            --poltina;
            grivna += 5;
        }
        else
        {
            ++poltina;
            grivna -= 5;
        }
    }

    while (abs(poltina) >= 2)
    {
        if (poltina < 0)
        {
            --rubl;
            poltina += 2;
        }
        else
        {
            ++rubl;
            poltina -= 2;
        }
    }

    this->rubl = rubl;
    this->poltina = poltina;
    this->grivna = grivna;
    this->altyn = altyn;
    this->kopeyka = kopeyka;
    this->denga = denga;
    this->polushka = polushka;
}

Money::Money(const Money &money)
{
    this->altyn = money.altyn;
    this->denga = money.denga;
    this->grivna = money.grivna;
    this->kopeyka = money.kopeyka;
    this->poltina = money.poltina;
    this->polushka = money.polushka;
    this->rubl = money.rubl;
}

Money::Money(long long kopeyka, long long denga, long long polushka)
{
    while (abs(polushka) >= 400)
    {
        if (polushka < 0)
        {
            --rubl;
            polushka += 400;
        }
        else
        {
            ++rubl;
            polushka -= 400;
        }
    }

    while (abs(polushka) >= 200)
    {
        if (polushka < 0)
        {
            --poltina;
            polushka += 200;
        }
        else
        {
            ++poltina;
            polushka -= 200;
        }
    }

    while (abs(polushka) >= 40)
    {
        if (polushka < 0)
        {
            --grivna;
            polushka += 40;
        }
        else
        {
            ++grivna;
            polushka -= 40;
        }
    }

    while (abs(polushka) >= 12)
    {
        if (polushka < 0)
        {
            --altyn;
            polushka += 12;
        }
        else
        {
            ++altyn;
            polushka -= 12;
        }
    }

    while (abs(polushka) >= 4)
    {
        if (polushka < 0)
        {
            --kopeyka;
            polushka += 4;
        }
        else
        {
            ++kopeyka;
            polushka -= 4;
        }
    }

    while (abs(polushka) >= 2)
    {
        if (polushka < 0)
        {
            --denga;
            polushka += 2;
        }
        else
        {
            ++denga;
            polushka -= 2;
        }
    }

    while (abs(denga) >= 200)
    {
        if (denga < 0)
        {
            --rubl;
            denga += 200;
        }
        else
        {
            ++rubl;
            denga -= 200;
        }
    }

    while (abs(denga) >= 100)
    {
        if (denga < 0)
        {
            --poltina;
            denga += 100;
        }
        else
        {
            ++poltina;
            denga -= 100;
        }
    }

    while (abs(denga) >= 20)
    {
        if (denga < 0)
        {
            --grivna;
            denga += 20;
        }
        else
        {
            ++grivna;
            polushka -= 20;
        }
    }

    while (abs(denga) >= 6)
    {
        if (denga < 0)
        {
            --altyn;
            denga += 6;
        }
        else
        {
            ++altyn;
            denga -= 6;
        }
    }

    while (abs(denga) >= 2)
    {
        if (denga < 0)
        {
            --kopeyka;
            denga += 2;
        }
        else
        {
            ++kopeyka;
            denga -= 2;
        }
    }

    while (abs(kopeyka) >= 100)
    {
        if (kopeyka < 0)
        {
            --rubl;
            kopeyka += 100;
        }
        else
        {
            ++rubl;
            kopeyka -= 100;
        }
    }

    while (abs(kopeyka) >= 50)
    {
        if (kopeyka < 0)
        {
            --poltina;
            kopeyka += 50;
        }
        else
        {
            ++poltina;
            kopeyka -= 50;
        }
    }

    while (abs(kopeyka) >= 10)
    {
        if (kopeyka < 0)
        {
            --grivna;
            kopeyka += 10;
        }
        else
        {
            ++grivna;
            kopeyka -= 10;
        }
    }

    while (abs(kopeyka) >= 3)
    {
        if (kopeyka < 0)
        {
            --altyn;
            kopeyka += 3;
        }
        else
        {
            ++altyn;
            kopeyka -= 3;
        }
    }

    this->kopeyka = kopeyka;
    this->denga = denga;
    this->polushka = polushka;
}

long long Money::getRubl() const
{
    return rubl;
}

long long Money::getPoltina() const
{
    return poltina;
}

long long Money::getGrivna() const
{
    return grivna;
}

int Money::getAltyn() const
{
    return altyn;
}

int Money::getKopeyka() const
{
    return kopeyka;
}

short Money::getDenga() const
{
    return denga;
}

short Money::getPolushka() const
{
    return polushka;
}

Money Money::operator+ (const Money &right)
{
    return Money(this->rubl + right.rubl,
                 this->poltina + right.poltina,
                 this->grivna + right.grivna,
                 this->altyn + right.altyn,
                 this->kopeyka + right.kopeyka,
                 this->denga + right.denga,
                 this->polushka + right.polushka);
}

Money Money::operator-(const Money &right)
{
    return Money(this->rubl - right.rubl,
                 this->poltina - right.poltina,
                 this->grivna - right.grivna,
                 this->altyn - right.altyn,
                 this->kopeyka - right.kopeyka,
                 this->denga - right.denga,
                 this->polushka - right.polushka);
}

Money &Money::operator+=(const Money &right)
{
    this->polushka += right.polushka;

    while (abs(this->polushka) >= 2)
    {
        if (this->polushka < 0)
        {
            --this->denga;
            this->polushka += 2;
        }
        else
        {
            ++this->denga;
            this->polushka -= 2;
        }
    }

    this->denga += right.denga;

    while (abs(this->denga) >= 2)
    {
        if (this->denga < 0)
        {
            --this->kopeyka;
            this->denga += 2;
        }
        else
        {
            ++this->kopeyka;
            this->denga -= 2;
        }
    }

    this->kopeyka += right.kopeyka;

    while (abs(this->kopeyka) >= 3)
    {
        if (this->kopeyka < 0)
        {
            --this->altyn;
            this->kopeyka += 3;
        }
        else
        {
            ++this->altyn;
            this->kopeyka -= 3;
        }
    }

    this->altyn += right.altyn;

    while (abs(this->altyn) >= 3 && abs(this->kopeyka) >= 1)
    {
        if (this->altyn < 0)
        {
            --this->grivna;
            ++this->kopeyka;
            this->altyn += 3;
        }
        else
        {
            ++this->grivna;
            --this->kopeyka;
            this->altyn -= 3;
        }
    }

    this->grivna += right.grivna;

    while (abs(this->grivna) >= 5)
    {
        if (this->grivna < 0)
        {
            --this->poltina;
            this->grivna += 5;
        }
        else
        {
            ++this->poltina;
            this->grivna -= 5;
        }
    }

    this->poltina += right.poltina;

    while (abs(this->poltina) >= 2)
    {
        if (this->poltina < 0)
        {
            --this->rubl;
            this->poltina += 2;
        }
        else
        {
            ++this->rubl;
            this->poltina -= 2;
        }
    }

    this->rubl += right.rubl;
    return *this;
}

Money &Money::operator-=(const Money &right)
{
    this->polushka += right.polushka;

    while (abs(this->polushka) >= 2)
    {
        if (this->polushka < 0)
        {
            --this->denga;
            this->polushka += 2;
        }
        else
        {
            ++this->denga;
            this->polushka -= 2;
        }
    }

    this->denga += right.denga;

    while (abs(this->denga) >= 2)
    {
        if (this->denga < 0)
        {
            --this->kopeyka;
            this->denga += 2;
        }
        else
        {
            ++this->kopeyka;
            this->denga -= 2;
        }
    }

    this->kopeyka += right.kopeyka;

    while (abs(this->kopeyka) >= 3)
    {
        if (this->kopeyka < 0)
        {
            --this->altyn;
            this->kopeyka += 3;
        }
        else
        {
            ++this->altyn;
            this->kopeyka -= 3;
        }
    }

    this->altyn += right.altyn;

    while (abs(this->altyn) >= 3 && abs(this->kopeyka) >= 1)
    {
        if (this->altyn < 0)
        {
            --this->grivna;
            ++this->kopeyka;
            this->altyn += 3;
        }
        else
        {
            ++this->grivna;
            --this->kopeyka;
            this->altyn -= 3;
        }
    }

    this->grivna += right.grivna;

    while (abs(this->grivna) >= 5)
    {
        if (this->grivna < 0)
        {
            --this->poltina;
            this->grivna += 5;
        }
        else
        {
            ++this->poltina;
            this->grivna -= 5;
        }
    }

    this->poltina += right.poltina;

    while (abs(this->poltina) >= 2)
    {
        if (this->poltina < 0)
        {
            --this->rubl;
            this->poltina += 2;
        }
        else
        {
            ++this->rubl;
            this->poltina -= 2;
        }
    }

    this->rubl += right.rubl;
    return *this;
}

bool Money::operator==(const Money &right)
{
    return  this->altyn == right.altyn &&
            this->denga == right.denga &&
            this->grivna == right.grivna &&
            this->kopeyka == right.kopeyka &&
            this->poltina == right.poltina &&
            this->polushka == right.polushka &&
            this->rubl == right.rubl;
}

bool Money::operator!=(const Money &right)
{
    return!(*this == right);
}

bool Money::operator<=(const Money &right)
{
    if (this->rubl < right.rubl)
        return 1;
    else
        if (this->rubl == right.rubl)
        {
            if (this->poltina < right.poltina)
                return 1;
            else
                if (this->poltina == right.poltina)
                {
                    if (this->grivna < right.grivna)
                        return 1;
                    else
                        if (this->grivna == right.grivna)
                        {
                            if (this->altyn < right.altyn)
                                return 1;
                            else
                                if (this->altyn == right.altyn)
                                {
                                    if (this->kopeyka < right.kopeyka)
                                        return 1;
                                    else
                                        if (this->kopeyka == right.kopeyka)
                                        {
                                            if (this->denga < right.denga)
                                                return 1;
                                            else
                                                if (this->denga == right.denga)
                                                    if (this->polushka <= right.polushka)
                                                        return 1;
                                        }
                                }
                        }
                }
        }
    return 0;
}

bool Money::operator>=(const Money &right)
{
    if (this->rubl > right.rubl)
        return 1;
    else
        if (this->rubl == right.rubl)
        {
            if (this->poltina > right.poltina)
                return 1;
            else
                if (this->poltina == right.poltina)
                {
                    if (this->grivna > right.grivna)
                        return 1;
                    else
                        if (this->grivna == right.grivna)
                        {
                            if (this->altyn > right.altyn)
                                return 1;
                            else
                                if (this->altyn == right.altyn)
                                {
                                    if (this->kopeyka > right.kopeyka)
                                        return 1;
                                    else
                                        if (this->kopeyka == right.kopeyka)
                                        {
                                            if (this->denga > right.denga)
                                                return 1;
                                            else
                                            {
                                                if (this->denga == right.denga)
                                                    if (this->polushka >= right.polushka)
                                                        return 1;
                                            }
                                        }
                                }
                        }
                }
        }
    return 0;
}

bool Money::operator<(const Money &right)
{
    if (this->rubl < right.rubl)
        return 1;
    else
        if (this->rubl == right.rubl)
        {
            if (this->poltina < right.poltina)
                return 1;
            else
                if (this->poltina == right.poltina)
                {
                    if (this->grivna < right.grivna)
                        return 1;
                    else
                        if (this->grivna == right.grivna)
                        {
                            if (this->altyn < right.altyn)
                                return 1;
                            else
                                if (this->altyn == right.altyn)
                                {
                                    if (this->kopeyka < right.kopeyka)
                                        return 1;
                                    else
                                        if (this->kopeyka == right.kopeyka)
                                        {
                                            if (this->denga < right.denga)
                                                return 1;
                                            else
                                                if (this->denga == right.denga)
                                                    if (this->polushka < right.polushka)
                                                        return 1;
                                        }
                                }
                        }
                }
        }
    return 0;
}

bool Money::operator>(const Money &right)
{
    if (this->rubl > right.rubl)
        return 1;
    else
        if (this->rubl == right.rubl)
        {
            if (this->poltina > right.poltina)
                return 1;
            else
                if (this->poltina == right.poltina)
                {
                    if (this->grivna > right.grivna)
                        return 1;
                    else
                        if (this->grivna == right.grivna)
                        {
                            if (this->altyn > right.altyn)
                                return 1;
                            else
                                if (this->altyn == right.altyn)
                                {
                                    if (this->kopeyka > right.kopeyka)
                                        return 1;
                                    else
                                        if (this->kopeyka == right.kopeyka)
                                        {
                                            if (this->denga > right.denga)
                                                return 1;
                                            else
                                                if (this->denga == right.denga)
                                                    if (this->polushka > right.polushka)
                                                        return 1;
                                        }
                                }
                        }
                }
        }
    return 0;;
}

Money &Money::operator=(const Money &money)
{
    //проверка на самоприсваивание
    if (this == &money)
        return *this;

    this->altyn = money.altyn;
    this->denga = money.denga;
    this->grivna = money.grivna;
    this->kopeyka = money.kopeyka;
    this->poltina = money.poltina;
    this->polushka = money.polushka;
    this->rubl = money.rubl;
    return *this;
}
