#include "Picture.h"

Picture::Picture(QString imgSrc)
{
    if (!imgSrc.isNull() && !imgSrc.isEmpty())
    {
        this->src = imgSrc;
    }

    calculateSize();
}

void Picture::setPic(const QString imgSrc)
{
    if (!imgSrc.isNull() && !imgSrc.isEmpty())
    {
        this->src = imgSrc;
    }

    calculateSize();
}

int Picture::getHeight() const
{
    return height;
}

int Picture::getWidth() const
{
    return width;
}

QString Picture::getStr(const bool isDiv) const
{
    if (isDiv)
        return QString("<div align=\"center\"><img src=\"%1\" width=\"%2\" height=\"%3\" /></div>").
                arg(src).arg(width).arg(height);
    else
        return QString("<img src=\"%1\" width=\"%2\" height=\"%3\" />").
                arg(src).arg(width).arg(height);
}

void Picture::calculateSize()
{
    if (textBrowser && !src.isNull() && !src.isEmpty())
    {
        if (textBrowser && !src.isNull() && !src.isEmpty())
        {
            coef = 0;
            coefW = 0;

            QPixmap pic(src);

            /*qDebug() << "Высота экрана = " << textBrowser->height()
                     <<"\tВысота картинки = " << pic.height();

            qDebug() << "Ширина экрана = " << textBrowser->width()
                     << "\tШирина картинки = " << pic.width();*/
            if (pic.height() > textBrowser->height() - textBrowser->font().pointSize() * 2)
            {
                coef = (double)pic.height() / double(textBrowser->height() - textBrowser->font().pointSize() * 2);
                height = pic.height() / coef;
            }
            else
            {
                height = pic.height();
            }
            if (pic.width() > textBrowser->width())
            {
                if (coef == 0)
                {
                    coefW = (double)pic.width() / double(textBrowser->width());
                    height /= coefW;
                    width = pic.width() / coefW;
                }
                else
                {
                    coefW = (double)pic.width() / double(textBrowser->width());
                    if (coefW > coef)
                    {
                        height = pic.height() / coefW;
                        width = pic.width() / coefW;
                    }
                    else
                    {
                        width = pic.width() / coef;
                    }
                }
            }
            else
            {
                if (coef == 0)
                    width = pic.width();
                else
                    width = pic.width() / coef;
            }
        }
        else
        {
            height = 0;
            width = 0;
        }
        /*qDebug() << "Конечная высота картинки = " << height
                 << "\tКонечная ширина картинки = " << width;
        qDebug() << "Коэффициент высоты = " << coef
                 << "\tКоэффициент ширины = " << coefW;*/
    }
}
