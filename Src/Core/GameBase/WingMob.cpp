#include "WingMob.h"

WingMob::WingMob()
{
    this->wingsSize     = 0;
    this->wingsGeometry = 0;
}

WingMob::WingMob(const WingMob *wingMob)
{
    this->wingsSize     = wingMob->wingsSize;
    this->wingsGeometry = wingMob->wingsGeometry;
}

unsigned short WingMob::getWingsSize() const
{
    return wingsSize;
}

void WingMob::setWingsSize(const unsigned short &size)
{
    this->wingsSize = size;
}

void WingMob::addSizeWings(const short &size)
{
    if (size > 0 || -size < this->wingsSize)
        this->wingsSize += size;
    else
        this->wingsSize = 0;
}

unsigned char WingMob::getWingsGeometry() const
{
    return wingsGeometry;
}

void WingMob::setWingsGeometry(const unsigned char &geometry)
{
    if (geometry >= maxWingsGeometry)
        this->wingsGeometry = maxWingsGeometry;
    else
        this->wingsGeometry = geometry;
}

QString WingMob::getNameGeometryWings() const
{
    return nameWingsGeometry[wingsGeometry];
}
