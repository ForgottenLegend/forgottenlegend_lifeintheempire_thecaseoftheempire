#ifndef CONVERTTOHTML_H
#define CONVERTTOHTML_H

#include <QString>
#include "../../Interface/AnimatedTextBrowser.h"

extern AnimatedTextBrowser *textBrowser;

class ConvertToHtml
{
public:
    ConvertToHtml();

    static QString getHeader(const QString &str, const bool isDiv = true);

    enum class Color {Red, Green, Blue, Purple, Black};
    static QString getColorStr(const QString &str, const Color &color);

    enum class Font:char {Italic = 1, Bold = 2, Underline = 4};
    static QString getFont(const QString &str, const char &font);

    enum class Margin:char {Left = 1, Right = 2, Top = 4, Bottom = 8};
    static QString getParagraph (const QString &str, const char &margin = 12);

    static QString getHtmlLocation(const QString &locanion, const QString id);

    static QString getLink(const QString &str, const QString &action);
};

#endif // CONVERTTOHTML_H
