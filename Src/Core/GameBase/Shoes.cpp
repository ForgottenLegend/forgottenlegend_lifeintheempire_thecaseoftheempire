#include "Shoes.h"

Shoes::Shoes(QString pic):
    Item()
{
    this->pic  = pic;
}

Shoes::Shoes(const Shoes *shoes):
    Item(shoes)
{
    this->pic  = shoes->pic;
}

QString Shoes::getStr()
{
    return Picture(pic).getStr();
}

