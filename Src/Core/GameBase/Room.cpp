#include "Room.h"

Room::Room(MainWindow *mw, Ui::MainWindow *ui, QObject *parent):
    Place(mw,ui,parent)
{
}

char Room::getStage()
{
    return stage;
}

void Room::setStage(const char stage)
{
    this->stage = stage;
}
