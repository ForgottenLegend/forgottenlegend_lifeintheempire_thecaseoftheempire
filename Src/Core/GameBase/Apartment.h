#ifndef APARTMENT_H
#define APARTMENT_H

#include <QObject>
#include "Place.h"

class MainWindow;
namespace Ui {
class MainWindow;
}

class Apartment : public Place
{
public:
    Apartment(MainWindow *mw, Ui::MainWindow *ui, QObject *parent=0);

//    virtual void entry(Place *parent = 0);

protected:
};

#endif // APARTMENT_H
