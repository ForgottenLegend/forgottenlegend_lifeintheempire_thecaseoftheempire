#include "Clothes.h"

Clothes::Clothes(QString picFront, bool part, QString picBack):
    Item()
{
    this->picFront  = picFront;
    this->picBack   = picBack;
    this->part      = part;
}

Clothes::Clothes(const Clothes *clothes):
    Item(clothes)
{
    this->picFront  = clothes->picFront;
    this->picBack   = clothes->picBack;
    this->part      = clothes->part;
}

QString Clothes::getStr()
{
    return QString("<div align=\"center\">%1%2</div>").arg(Picture(picFront).getStr(0)).arg(Picture(picBack).getStr(0));
}
