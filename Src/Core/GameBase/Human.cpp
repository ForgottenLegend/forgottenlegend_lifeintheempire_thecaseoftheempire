#include "Human.h"

Human::Human() :
    Humanoid()
{
    this->reputation        = 0;
    this->fame              = 0;
    this->secondName        = "";
    this->lastName          = "";
    this->shortName         = "";
    this->capacityThroat    = 0;
    this->horny             = 0;
    this->sexExp            = 0;
    this->riches            = 0;
    calculateMaxWill();
    this->will = maxWill;
}

Human::Human(const Human *human) :
    Humanoid(human)
{
    this->reputation        = human->reputation;
    this->fame              = human->fame;
    this->secondName        = human->secondName;
    this->lastName          = human->lastName;
    this->shortName         = human->shortName;
    this->capacityThroat    = human->capacityThroat;
    this->horny             = human->horny;
    this->sexExp            = human->sexExp;
    this->riches            = human->riches;
    this->maxWill           = human->maxWill;
    this->will              = human->will;
}

unsigned int Human::getReputation() const
{
    return reputation;
}

void Human::setReputation(const int &reputation)
{
    if (reputation > maxReputation)
        this->reputation = maxReputation;
    else
        if (reputation < minReputation)
            this->reputation = minReputation;
        else
            this->reputation = reputation;
}

void Human::addReputation(const short &reputation)
{
    this->reputation += reputation;
    if (this->reputation > maxReputation)
        this->reputation = maxReputation;
    else
        if (this->reputation < minReputation)
            this->reputation = minReputation;
}

unsigned int Human::getFame() const
{
    return fame;
}

void Human::setFame(const unsigned int &fame)
{
    if (fame > maxFame)
        this->fame = maxFame;
    else
        this->fame = fame;
}

void Human::addFame(const int &fame)
{
    if (fame >= 0 || -fame <= this->fame)
        this->fame += fame;
    else
        this->fame = 0;
    if (this->fame > maxFame)
        this->fame = maxFame;
}

QString Human::getSecondName() const
{
    return secondName;
}

void Human::setSecondName(const QString &secondName)
{
    this->secondName = secondName;
}

QString Human::getLastName() const
{
    return lastName;
}

void Human::setLastName(const QString &lastName)
{
    this->lastName = lastName;
}

QString Human::getShortName() const
{
    return shortName;
}

void Human::setShortName(const QString &shortName)
{
    this->shortName = shortName;
}

unsigned char Human::getCapacityThroat() const
{
    return capacityThroat;
}

void Human::setCapacityThroat(const unsigned char &capacity)
{
    if (capacity > maxCapacityThroat)
        this->capacityThroat = maxCapacityThroat;
}

void Human::addCapacityThroat(const signed char &capacity)
{
    if (capacity > 0 || -capacity < this->capacityThroat)
        this->capacityThroat += capacity;
    else
        if (capacityThroat > 0)
            this->capacityThroat = 1;
    if (this->capacityThroat > maxCapacityThroat)
        this->capacityThroat = maxCapacityThroat;
}

unsigned char Human::getHorhy() const
{
    return horny;
}

void Human::setHorny(const unsigned char &horny)
{
    if (horny > maxHorny)
        this->horny = maxHorny;
    else
        this->horny = horny;
}

void Human::addHorny(const signed char &horny)
{
    if (horny > 0)
        if (this->horny + horny > maxHorny)
            this->horny = maxHorny;
        else
            this->horny += horny;
    else
        if (this->horny >= -horny)
            this->horny += horny;
        else
            this->horny = 0;
}

unsigned long long Human::getSexExp() const
{
    return sexExp;
}

void Human::setSexExp(const unsigned long long &sexExp)
{
    this->sexExp = sexExp;
}

void Human::addSexExp(const long long &sexExp)
{
    if (sexExp > 0 || -sexExp < this->sexExp)
        this->sexExp += sexExp;
    else
        this->sexExp = 0;
}

signed char Human::getRiches() const
{
    return riches;
}

void Human::setRiches(const signed char &riches)
{
    this->riches = riches;
}

void Human::setRace(const unsigned int &race)
{
    Humanoid::setRace(race);
    if (this->race > maxRace)
        this->race = maxRace;
}

void Human::addRace(const long long &race)
{
    Humanoid::addRace(race);
    if (this->race > maxRace)
        this->race = maxRace;
}

void Human::setStrength(const unsigned int &strength)
{
    Humanoid::setStrength(strength);
    if (this->strength > maxStrength)
        this->strength = maxStrength;
}

void Human::addStrength(const long long &strength)
{
    Humanoid::addStrength(strength);
    if (this->strength > maxStrength)
        this->strength = maxStrength;
}

void Human::setDexterity(const unsigned int &dexterity)
{
    Humanoid::setDexterity(dexterity);
    if (this->dexterity > maxDexterity)
        this->dexterity = maxDexterity;
}

void Human::addDexterity(const long long &dexterity)
{
    Humanoid::addDexterity(dexterity);
    if (this->dexterity > maxDexterity)
        this->dexterity = maxDexterity;
}

void Human::setEndurance(const unsigned int &endurance)
{
    Humanoid::setEndurance(endurance);
    if (this->endurance > maxEndurance)
        this->endurance = maxEndurance;
}

void Human::addEndurance(const long long &endurance)
{
    Humanoid::addEndurance(endurance);
    if (this->endurance > maxEndurance)
        this->endurance = maxEndurance;
}

void Human::setReaction(const unsigned int &reaction)
{
    Humanoid::setReaction(reaction);
    if (this->reaction > maxReaction)
        this->reaction = maxReaction;
}

void Human::addReaction(const long long &reaction)
{
    Humanoid::addReaction(reaction);
    if (this->reaction > maxReaction)
        this->reaction = maxReaction;
}

void Human::setIntellect(const unsigned int &intellect)
{
    Humanoid::setIntellect(intellect);
    if (this->intellect > maxIntellect)
        this->intellect = maxIntellect;
    calculateMaxWill();
}

void Human::addIntellect(const long long &intellect)
{
    Humanoid::addIntellect(intellect);
    if (this->intellect > maxIntellect)
        this->intellect = maxIntellect;

    calculateMaxWill();
}

void Human::setMind(const unsigned int &mind)
{
    Humanoid::setMind(mind);
    if (this->mind > maxMind)
        this->mind = maxMind;

    calculateMaxWill();
}

void Human::addMind(const long long &mind)
{
    Humanoid::addMind(mind);
    if (this->mind > maxMind)
        this->mind = maxMind;

    calculateMaxWill();
}

unsigned int Human::getMaxWill() const
{
    return maxWill;
}

void Human::calculateMaxWill()
{
    maxWill += level * 2 + 5 + intellect / 50 + mind / 250;
}
