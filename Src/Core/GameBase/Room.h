#ifndef ROOM_H
#define ROOM_H

#include "Place.h"

class Room : public Place
{
public:
    explicit Room(MainWindow *mw, Ui::MainWindow *ui, QObject *parent=0);

    char getStage();
    void setStage(const char stage);

protected:
    char stage;
};

#endif // ROOM_H
