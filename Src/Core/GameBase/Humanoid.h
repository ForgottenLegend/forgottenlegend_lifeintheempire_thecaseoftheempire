#ifndef HUMANOID_H
#define HUMANOID_H

#include "Mob.h"

class Humanoid : public Mob
{
public:
    Humanoid();
    Humanoid(const Humanoid *humanoid);

    unsigned int getAge() const;
    void setAge (const unsigned int &age);
    virtual void addAge (const int &age);

    unsigned int getApparentAge () const;
    void setApparentAge (const unsigned int &apparentAge);
    virtual void addApparentAge (const int &apparentAge);

    unsigned char getTitsSize () const;
    void setTitsSize (const unsigned char &titsSize);
    void addTitsSIze (const short &titsSize);
    QString getNameTitsSize() const;

    void setGender (Gender gender);
    virtual void setGender(Gender gender, const unsigned char titsSize);

    long long getDominate() const;
    void setDominate (const long long &dominate);
    void addDominate (const long long &dominate);

    unsigned int getWill() const;
    void setWill (const unsigned int &will);
    void addWill (const long long  &will);

protected:
    unsigned int age, apparentAge;
    unsigned char titsSize;

    long long dominate;

    unsigned int will; //воля

    const unsigned char maxTitsSize = 7;

    const long long minDominate = -2000,
                    maxDominate = 2000;

    const unsigned int maxWill = 10000;

    const QString nameTitsSize[8] = {"AA", "A", "B", "C", "D", "DD", "E", "F"};

    virtual void calculateMaxClearMind();
};

#endif // HUMANOID_H
