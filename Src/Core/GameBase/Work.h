#ifndef WORK_H
#define WORK_H

#include "GameObject.h"

class Work : public GameObject
{
public:
    Work();

    unsigned int getLabourWorkExperience() const;
    void setLabourWorkExperience (const unsigned int &labourWorkExperience);
    void addLabourWorkExperience (const int &labourWorkExperience);

protected:
    unsigned int labourWorkExperience;
};

#endif // WORK_H
