#include "Date.h"

Date::Date(QObject *parent) :
    QObject(parent)
{
}

unsigned char Date::getHour() const
{
    return hour;
}

void Date::setHour(short h)
{
    while (h >= 24)
    {
        unsigned long long buf = dayOfStart + 1;
        setDayOfTheStart(buf);
        h-=24;
    }
    while (h<0)
    {
        unsigned long long buf = dayOfStart - 1;
        setDayOfTheStart(buf);
        h+=24;
    }
    hour = h;
}

void Date::addHour(short h)
{
    short   dh = h,
            dd = 0;
    while (h +hour >= 24)
    {
//        addDayOfTheMonth(1);
//        addDayOfTheWeek(1);
//        setDayOfTheYear(dayOfTheYear+1);
//        addDayOfTheStart(1);
        h-=24;
        ++dd;
    }
    while (h + hour <0)
    {
//        addDayOfTheMonth(-1);
//        addDayOfTheWeek(-1);
//        setDayOfTheYear(dayOfTheYear-1);
//        addDayOfTheStart(-1);
        h+=24;
        --dd;
    }
    hour+=h;

    unsigned long long buf;
    if (dd >= 0 || -dd < dayOfStart)
        buf = dayOfStart + dd;
    else
        buf = 0;
    setDayOfTheStart(buf);

    emit changeDayOfStart(dd);
    emit changeHour(dh);
    emit changeMinute(dh * 60);
    emit changeSecond(dh * 3600);
}

unsigned char Date::getMinute() const
{
    return minute;
}

void Date::setMinute(short m)
{
    while (m>=60)
    {
        setHour(hour+1);
        m-=60;
    }
    while (m < 0)
    {
        setHour(hour-1);
        m+=60;
    }
    minute = m;
}

void Date::addMinute(short m)
{
    short   bufHour = 0,
            bufMin = m,
            bufDay = 0;
    while (m + minute>=60)
    {
        ++bufHour;
        m -= 60;
    }
    while (m + minute < 0)
    {
        --bufHour;
        m += 60;
    }
    minute += m;

    setHour(hour + bufHour);

    bufDay = bufHour/24;

    if (bufDay != 0)
        emit changeDayOfStart(bufDay);
    if (bufHour != 0)
        emit changeHour(bufHour);
    if (bufMin != 0)
        emit changeMinute(bufMin);
    emit changeSecond(bufMin * 60);
}

unsigned char Date::getSecond() const
{
    return second;
}

void Date::setSecond(short s)
{
    while (s >= 60)
    {
        setMinute(minute + 1);
        s -= 60;
    }
    while (s < 0)
    {
        setMinute(minute - 1);
        s += 60;
    }
    second = s;
}

void Date::addSecond(short s)
{
    short bufSec = s,
            bufMin = 0,
            bufHour = 0;
    while (s + second >= 60)
    {
        setMinute(minute + 1);
        ++bufMin;
        if (bufMin >=60)
        {
            ++bufHour;
            bufMin -= 60;
        }
        s -= 60;
    }
    while (s + second < 0)
    {
        setMinute(minute - 1);
        --bufMin;
        if (bufMin < 0)
        {
            --bufHour;
            bufMin += 60;
        }
        s += 60;
    }
    second += s;

    short bufDay = bufHour / 24;

    if (bufDay != 0)
        emit changeDayOfStart(bufDay);
    if (bufHour != 0)
        emit changeHour(bufHour);
    if (bufHour != 0 || bufMin != 0)
        emit changeMinute(bufHour * 60 + bufMin);
    if (bufSec != 0)
        emit changeSecond(bufSec);
}

unsigned char Date::getDayOfTheWeek() const
{
    return dayOfTheWeek;
}

void Date::setDayOfTheWeek(short day)
{
    while (day >= 7)
    {
        day-=7;
    }
    while (day < 0)
    {
        day+=7;
    }
    dayOfTheWeek=day;
}

void Date::addDayOfTheWeek(const short &day)
{
    setDayOfTheWeek(dayOfTheWeek+day);
}

unsigned char Date::getDayOfTheMonth() const
{
    return dayOfTheMonth;
}

void Date::setDayOfTheMonth(short day)
{
    bool flag;
    do
    {
        flag =0;
        if ((day >= 31)&&(month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11))
        {
            setMonth(month+1);
            day -=31;
            flag = 1;
        }
        if ((day >= 30)&&(month == 3 || month == 5 || month == 8 || month == 10))
        {
            setMonth(month+1);
            day -=30;
            flag = 1;
        }
        if ((day < 0)&&(month == 4 || month == 6 || month == 7 || month == 9 || month == 11))
        {
            setMonth(month-1);
            day +=30;
            flag = 1;
        }
        if ((day < 0)&&(month == 0 || month == 1 || month == 3 || month == 5 || month == 8 || month == 10))
        {
            setMonth(month-1);
            day +=31;
            flag = 1;
        }
        if (year%4==0)
        {
            if (month == 1 && day >= 29)
            {
                setMonth(month+1);
                day -= 29;
                flag = 1;
            }
            if (month == 2 && day < 0)
            {
                setMonth(month-1);
                day += 29;
                flag = 1;
            }
        }
        else
        {
            if (month == 1 && day >= 28)
            {
                setMonth(month+1);
                day -= 28;
                flag = 1;
            }
            if (month == 2 && day < 0)
            {
                setMonth(month-1);
                day += 28;
                flag = 1;
            }
        }
    }while(flag);
    dayOfTheMonth = day;
}

void Date::addDayOfTheMonth(const short &day)
{
    setDayOfTheMonth(dayOfTheMonth + day);
}

unsigned char Date::getMonth() const
{
    return month;
}

void Date::setMonth(short m)
{
    while (m >= 12)
    {
        setYear(year+1);
        m-=12;
    }
    while (m <0)
    {
        setYear(year-1);
        m+=12;
    }
    month = m;
    if (month >= 2 && month <= 4)
    {
        setSeason(0);
    }
    else
        if (month >= 5 && month <= 7)
        {
            setSeason(1);
        }
        else
            if (month >= 8 && month <= 10)
            {
                setSeason(2);
            }
            else
                if (month == 11 || month <= 1)
                {
                    setSeason(3);
                }
}

void Date::addMonth(const short &m)
{
    setMonth(month + m);
}

unsigned char Date::getSeason() const
{
    return season;
}

void Date::setSeason(short s)
{
    while (s > 3)
        s-=4;
    while (s < 0)
        s+=4;
    season=s;
}

/*unsigned short Season::detDayOfTheYear()
{
    return dayOfTheYear;
}

void Season::setDayOfTheYear(int day)
{
    while (day >= dayInTheYear)
    {
        setYear(year + 1);
        day -=dayInTheYear;
    }
    while (day < 0)
    {
        setYear(year - 1);
        day +=dayInTheYear;
    }
    dayOfTheYear=day;
}*/

long long Date::getYear() const
{
    return year;
}

void Date::setYear(const long long &year)
{
    this->year=year;
}

void Date::addYear(const long long &year)
{
    this->year+=year;
}

unsigned long long Date::getDayOfTheStart() const
{
    return dayOfStart;
}

void Date::setDayOfTheStart(unsigned long long &day)
{
//    if (dayOfStart > day)
//        emit changeDayOfStart(1);
//    else
//        if (dayOfStart < day)
//            emit changeDayOfStart(-1);
//        else
//            emit changeDayOfStart(0);
    if (dayOfStart > day)
    {
        addDayOfTheMonth(dayOfStart - day);
        addDayOfTheWeek(dayOfStart - day);
    }
    else
    {
        addDayOfTheMonth(day - dayOfStart);
        addDayOfTheWeek(day - dayOfStart);
    }
    dayOfStart = day;
}

void Date::addDayOfTheStart(const long long &day)
{
    if ((day + dayOfStart) <= 0)
        dayOfStart = 0;
    else
        dayOfStart+=day;

    addDayOfTheWeek(day);
    addDayOfTheMonth(day);

    emit changeDayOfStart(day);
}

QString Date::getNameMonth() const
{
    if (month < 12)
        return nameMonth[month];
    else
        return "";
}

QString Date::getNameDayOfTheWeek() const
{
    if(dayOfTheWeek <7)
        return nameDayOfTheWeek[dayOfTheWeek];
    else
        return "";
}

QString Date::getNameSeason() const
{
    if (season <=3)
        return nameSeason[season];
    else
        return "";
}
