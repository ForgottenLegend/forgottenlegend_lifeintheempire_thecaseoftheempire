#ifndef OLDWEATHER_H
#define OLDWEATHER_H

#include <QObject>

#include "OldDate.h"

class OldWeather : public QObject
{
    Q_OBJECT
public:
    explicit OldWeather(OldDate *date, QObject *parent = 0);

    bool isRain() const;
    bool isSnow() const;

    int getTemperature() const;
signals:
    void changeWeather();

private slots:
    void calculateWeather(signed char h);

private:
    bool flagRain;
    bool flagSnow;
    int temperature;

    OldDate *date;

//    const signed char minTemperature[9] = {-40, -30, -20, -10, -5,  5,  5, -5, -10, -20, -30, -40};
//    const signed char maxTemperature[9] = {5,    10,  20,  30, 35, 40, 40, 35,  30,  20,  10,   5};
};

#endif // OLDWEATHER_H
