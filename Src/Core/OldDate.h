#ifndef OLDDATE_H
#define OLDDATE_H

#include <QObject>

class Settings;

class OldDate : public QObject
{
    Q_OBJECT
public:
    explicit OldDate(QObject *parent = 0);
    explicit OldDate(OldDate *oldDate, QObject *parent = 0);
    explicit OldDate(unsigned char hour, unsigned char houring, unsigned char dayOfTheWeek, unsigned char dayOfTheSorokovnik,
                     unsigned char sorokovnik, long long year, QObject *parent = 0);

    unsigned char getHour() const;
    void setHour(short hour);
    void addHour(short hour);

    unsigned char getHouring() const;
    void setHouring(short houring);
    void addHouring(short houring);

    unsigned char getSecond() const;
    void setSecond(short s);
    void addSecond(short s);

    unsigned char getDayOfTheWeek() const;
    void setDayOfTheWeek(short day);
    void addDayOfTheWeek(const short &day);

    unsigned char getDayOfTheSorokovnik() const;
    void setDayOfTheSorokovnik(short day);
    void addDayOfTheSorokovnik(const short &day);

    unsigned char getSorokovnik() const;
    void setSorokovnik(short m);
    void addSorokovnik(const short &m);

    unsigned char getSeason() const;
    void setSeason(short s);
    void calculateSeason();

    long long getYear() const;
    void setYear(const long long &year);
    void addYear(const long long &year);

    unsigned long long getDayOfTheStart() const;
    void setDayOfTheStart(unsigned long long &day);
    void addDayOfTheStart(const long long &day);

    QString getNameSorokovnik() const;

    QString getNameDayOfTheWeek() const;

    QString getNameSeason() const;

    bool isBirthday(const OldDate &date) const;

    unsigned short getHourInTheDay();
    unsigned short getHouringInHour();

    friend class Settings;
signals:
    void changeDayOfStart(signed int day);

    void changeHour(signed int hour);

    void changeHouring(long long houring);

public slots:

private:
    unsigned char   hour,
                    houring,
                    dayOfTheWeek,
                    dayOfTheSorokovnik,
                    sorokovnik,
                    season;

    long long year;
    unsigned long long dayOfStart;



    const QString nameSorokovnik[9] = {tr("Рамхатъ"), tr("Айлетъ"),tr("Бейлетъ"),tr("Гэйлетъ"),tr("Дайлетъ"),tr("Элетъ"),
                                  tr("Вэйлетъ"),tr("Хейлетъ"),tr("Тайлетъ")};

    const QString nameDayOfTheWeek[9] = {tr("Понедельникъ"), tr("Вторникъ"), tr("Третейникъ"), tr("Четверикъ"),
                                         tr("Пятница"), tr("Шестица"), tr("Седьмица"), tr("Осьмица"), tr("Неделя")};

    const QString nameSeason[3] = {tr("Оусень"), tr("Зима"), tr("Весна")};

    const QString nameHour[16] = {tr("Паобедъ"), tr("Вечиръ"), tr("Ничь"), tr("Полничь"), tr("Заутра"), tr("Заура"),
                                 tr("Заурнице"), tr("Настя"), tr("Сваор"), tr("Утрось"), tr("Поутрось"),
                                 tr("Обестина"), tr("Обесть"), tr("Подани"), tr("Утдайни"), tr("Поудани")};
    /* 1    19.30
     * 2    21.00
     * 3    22.30
     * 4    24.00
     * 5    01.30
     * 6    03.00
     * 7    04.30
     * 8    06.00
     * 9    07.30
     * 10   09.00
     * 11   10.30
     * 12   12.00
     * 13   13.30
     * 14   15.00
     * 15   16.30
     * 16   18.00
     */

    const unsigned short    hourInTheDay = 16,
                            houringInHour = 144,    //90 минут
                            dolInHouring = 1298,
                            mgnovenieInDol = 72,
                            migInMgnovenie = 760,
                            sigInMig = 160,         //1 сек = 1888102.236 мигов
                            sangigInSig = 14000;    //1 сек = 302096358 сигов

    const unsigned char     dayInWeek = 9,
                            dayInFullSorokovnik = 41,
                            dayInPartialSorokovnik = 40,
                            monthInYear = 9,
                            seasonInYear = 3;
};

#endif // OLDDATE_H
