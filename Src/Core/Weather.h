#ifndef WEATHER_H
#define WEATHER_H

#include <QObject>
#include "Date.h"

class Weather : public QObject
{
    Q_OBJECT
public:
    explicit Weather(Date *date, QObject *parent = 0);

    bool isRain() const;
    bool isSnow() const;

    int getTemperature() const;
signals:
    void changeWeather();

private slots:
    void calculateWeather(signed char h);

private:
    bool flagRain;
    bool flagSnow;
    int temperature;

    Date *date;

    const signed char minTemperature[12] = {-40, -30, -20, -10, -5,  5,  5, -5, -10, -20, -30, -40};
    const signed char maxTemperature[12] = {5,    10,  20,  30, 35, 40, 40, 35,  30,  20,  10,   5};
};

#endif // WEATHER_H
