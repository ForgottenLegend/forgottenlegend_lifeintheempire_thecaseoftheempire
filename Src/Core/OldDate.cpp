#include "OldDate.h"

OldDate::OldDate(QObject *parent) :
    QObject(parent)
{
}

OldDate::OldDate(OldDate *oldDate, QObject *parent) :
    QObject(parent)
{
    this->hour                  = oldDate->hour;
    this->houring               = oldDate->houring;
    this->dayOfTheWeek          = oldDate->dayOfTheWeek;
    this->dayOfTheSorokovnik    = oldDate->dayOfTheSorokovnik;
    this->sorokovnik            = oldDate->sorokovnik;
    this->season                = oldDate->season;
    this->year                  = oldDate->year;
    this->dayOfStart            = oldDate->dayOfStart;
}

OldDate::OldDate(unsigned char hour, unsigned char houring, unsigned char dayOfTheWeek, unsigned char dayOfTheSorokovnik,
                 unsigned char sorokovnik, long long year, QObject *parent) :
    QObject(parent)
{
    this->hour = hour;
    this->houring = houring;
    this->dayOfTheWeek = dayOfTheWeek;
    this->dayOfTheSorokovnik = dayOfTheSorokovnik;
    this->sorokovnik = sorokovnik;
    this->year = year;
    calculateSeason();
}

unsigned char OldDate::getHour() const
{
    return hour;
}

void OldDate::setHour(short hour)
{
    while (hour >= hourInTheDay)
    {
        unsigned long long buf = dayOfStart + 1;
        setDayOfTheStart(buf);
        hour -= hourInTheDay;
    }
    while (hour < 0)
    {
        unsigned long long buf = dayOfStart - 1;
        setDayOfTheStart(buf);
        hour += hourInTheDay;
    }
    this->hour = hour;
}

void OldDate::addHour(short hour)
{
    short   dh = hour,
            dd = 0;
    while (this->hour + hour >= hourInTheDay)
    {
        hour -= hourInTheDay;
        ++dd;
    }
    while (hour + this->hour < 0)
    {
        hour += hourInTheDay;
        --dd;
    }
    this->hour+=hour;

    unsigned long long buf;
    if (dd >= 0 || -dd < dayOfStart)
        buf = dayOfStart + dd;
    else
        buf = 0;
    setDayOfTheStart(buf);

    emit changeDayOfStart(dd);
    emit changeHour(dh);
    emit changeHouring(dh * houringInHour);
}

unsigned char OldDate::getHouring() const
{
    return houring;
}

void OldDate::setHouring(short houring)
{
    while (houring >= houringInHour)
    {
        setHour(hour + 1);
        houring -= houringInHour;
    }
    while (houring < 0)
    {
        setHour(hour - 1);
        houring += houringInHour;
    }
    this->houring = houring;
}

void OldDate::addHouring(short houring)
{
    short   bufHour = 0,
            bufHouring = houring,
            bufDay = 0;
    while (houring + this->houring >= houringInHour)
    {
        ++bufHour;
        houring -= houringInHour;
    }
    while (houring + this->houring < 0)
    {
        --bufHour;
        houring += houringInHour;
    }
    this->houring += houring;

    setHour(hour + bufHour);

    bufDay = bufHour / hourInTheDay;

    if (bufDay != 0)
        emit changeDayOfStart(bufDay);
    if (bufHour != 0)
        emit changeHour(bufHour);
    if (bufHouring != 0)
        emit changeHouring(bufHouring);
}

unsigned char OldDate::getDayOfTheWeek() const
{
    return dayOfTheWeek;
}

void OldDate::setDayOfTheWeek(short day)
{
    while (day >= dayInWeek)
    {
        day -= dayInWeek;
    }
    while (day < 0)
    {
        day += dayInWeek;
    }
    dayOfTheWeek = day;
}

void OldDate::addDayOfTheWeek(const short &day)
{
    setDayOfTheWeek(dayOfTheWeek + day);
}

unsigned char OldDate::getDayOfTheSorokovnik() const
{
    return dayOfTheSorokovnik;
}

void OldDate::setDayOfTheSorokovnik(short day)
{
    bool flag;
    do
    {
        flag =0;
        if ((sorokovnik / 2 == 0 || year / 16 == 0) && day >= dayInFullSorokovnik)
        {
            setSorokovnik(sorokovnik + 1);
            day -= dayInFullSorokovnik;
            flag = 1;
        }
        else
            if((sorokovnik / 2 == 1 && year / 16 == 1) && day >= dayInPartialSorokovnik)
            {
                setSorokovnik(sorokovnik + 1);
                day -= dayInPartialSorokovnik;
                flag = 1;
            }
            else
                if (day < 0 && (sorokovnik / 2 == 0 || year / 16 == 0))
                {
                    setSorokovnik(sorokovnik - 1);
                    day += dayInFullSorokovnik;
                    flag = 1;
                }
                else
                    if(day < 0 && (sorokovnik / 2 == 1 && year / 16 == 1))
                    {
                        setSorokovnik(sorokovnik - 1);
                        day += dayInPartialSorokovnik;
                        flag = 1;
                    }
    }while(flag);
    dayOfTheSorokovnik = day;
}

void OldDate::addDayOfTheSorokovnik(const short &day)
{
    setDayOfTheSorokovnik(dayOfTheSorokovnik + day);
}

unsigned char OldDate::getSorokovnik() const
{
    return sorokovnik;
}

void OldDate::setSorokovnik(short m)
{
    while (m >= monthInYear)
    {
        setYear(year + 1);
        m -= monthInYear;
    }
    while (m < 0)
    {
        setYear(year - 1);
        m += monthInYear;
    }
    sorokovnik = m;
    if (sorokovnik >= 0 && sorokovnik <= 1 || sorokovnik == 8)
    {
        setSeason(0);
    }
    else
        if (sorokovnik >= 2 && sorokovnik <= 4)
        {
            setSeason(1);
        }
        else
            if (sorokovnik >= 5 && sorokovnik <= 7)
            {
                setSeason(2);
            }
}

void OldDate::addSorokovnik(const short &m)
{
    setSorokovnik(sorokovnik + m);
}

unsigned char OldDate::getSeason() const
{
    return season;
}

void OldDate::setSeason(short s)
{
    while (s >= seasonInYear)
        s -= seasonInYear;
    while (s < 0)
        s += seasonInYear;
    season = s;
}

void OldDate::calculateSeason()
{
    if ((sorokovnik >= 0 && sorokovnik <= 1) || sorokovnik == 8)
    {
        setSeason(0);
    }
    else
        if (sorokovnik >= 2 && sorokovnik <= 4)
        {
            setSeason(1);
        }
        else
            if (sorokovnik >= 5 && sorokovnik <= 7)
            {
                setSeason(2);
            }
}

long long OldDate::getYear() const
{
    return year;
}

void OldDate::setYear(const long long &year)
{
    this->year = year;
}

void OldDate::addYear(const long long &year)
{
    this->year += year;
}

unsigned long long OldDate::getDayOfTheStart() const
{
    return dayOfStart;
}

void OldDate::setDayOfTheStart(unsigned long long &day)
{
    if (dayOfStart > day)
    {
        addDayOfTheSorokovnik(dayOfStart - day);
        addDayOfTheWeek(dayOfStart - day);
    }
    else
    {
        addDayOfTheSorokovnik(day - dayOfStart);
        addDayOfTheWeek(day - dayOfStart);
    }
    dayOfStart = day;
}

void OldDate::addDayOfTheStart(const long long &day)
{
    if ((day + dayOfStart) <= 0)
        dayOfStart = 0;
    else
        dayOfStart+=day;

    addDayOfTheWeek(day);
    addDayOfTheSorokovnik(day);

    emit changeDayOfStart(day);
}

QString OldDate::getNameSorokovnik() const
{
    if (sorokovnik < monthInYear)
        return nameSorokovnik[sorokovnik];
    else
        return "";
}

QString OldDate::getNameDayOfTheWeek() const
{
    if(dayOfTheWeek < dayInWeek)
        return nameDayOfTheWeek[dayOfTheWeek];
    else
        return "";
}

QString OldDate::getNameSeason() const
{
    if (season < seasonInYear)
        return nameSeason[season];
    else
        return "";
}

bool OldDate::isBirthday(const OldDate &date) const
{
    if (this == &date)
        return 1;
    if (    this->dayOfTheSorokovnik == date.dayOfTheSorokovnik &&
            this->sorokovnik == date.sorokovnik)
        return 1;
    else
        return 0;
}

unsigned short OldDate::getHourInTheDay()
{
    return hourInTheDay;
}

unsigned short OldDate::getHouringInHour()
{
    return houringInHour;
}
