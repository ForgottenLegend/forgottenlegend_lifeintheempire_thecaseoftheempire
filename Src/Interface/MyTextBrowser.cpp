#include "MyTextBrowser.h"

MyTextBrowser::MyTextBrowser(QWidget *parent) :
    QTextBrowser(parent)
{
}

void MyTextBrowser::append(const QString &text)
{
    QString qs = this->toHtml();
    int buf = qs.indexOf("</body>");
    if (buf > 0)
    {
        qs.insert(buf, text);
        this->setHtml(qs);
        return;
    }
    else if ((buf = qs.indexOf("</html>")) > 0)
    {
        qs.insert(buf, text);
        this->setHtml(qs);
        return;
    }
    else
        QTextBrowser::append(text);
    qDebug() << qs;
}

void MyTextBrowser::insertHtml(const QString &text)
{
    QString qs = this->toHtml();
    int buf = qs.indexOf("</body>");
    if (buf > 0)
    {
        qs.insert(buf, text);
        this->setHtml(qs);
        return;
    }
    else if ((buf = qs.indexOf("</html>")) > 0)
    {
        qs.insert(buf, text);
        this->setHtml(qs);
        return;
    }
    else
        QTextBrowser::insertHtml(text);
//    qDebug() << qs;
}
