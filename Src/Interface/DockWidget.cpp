#include "DockWidget.h"

DockWidget::DockWidget(const QString &title, QWidget *parent, Qt::WindowFlags flags) :
    QDockWidget(title, parent, flags)
{
}

DockWidget::DockWidget(QWidget *parent, Qt::WindowFlags flags) :
    QDockWidget(parent, flags)
{
}

void DockWidget::resize(const QSize &size)
{
    oldMaxSize = this->maximumSize();
    oldMinSize = this->minimumSize();

    /*if (size.width() >= 0)
        if (this->width() < size.width())
            this->setMinimumWidth(size.width());
        else
            this->setMaximumWidth(size.width());
    if (size.height() >= 0)
        if (this->height() < size.height())
            this->setMinimumHeight(size.height());
        else
            this->setMaximumHeight(size.height());*/

    this->setMaximumSize(size);
    this->setMinimumSize(size);

    qDebug() << this->windowTitle();
    qDebug() << "oldMaxSize = " << oldMaxSize;
    qDebug() << "oldMinSize = " << oldMinSize;
    qDebug() << "MaxSize = " << maximumSize();
    qDebug() << "minSize = " << minimumSize();

    QTimer::singleShot(1, this, SLOT(returnToOldMaxMinSizes()));
}

/*void DockWidget::setSize(const QSize &size)
{
    this->mySize = size;
}*/

/*QSize DockWidget::sizeHint() const
{
    QSize sh = QDockWidget::sizeHint();
    if (mySize.height() >= 0 && mySize.width() >= 0)
    {
        sh.setHeight(mySize.height());
        sh.setWidth(mySize.width());
    }
    return sh;
}*/

void DockWidget::returnToOldMaxMinSizes()
{
    this->setMinimumSize(oldMinSize);
    this->setMaximumSize(oldMaxSize);
}
