#ifndef INPUTDIALOG_H
#define INPUTDIALOG_H

//#include <QDialogButtonBox>
#include <QAbstractButton>
#include <QDialog>

namespace Ui {
class InputDialog;
}

class InputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputDialog(const QString &caption, const QString &text);
    ~InputDialog();

    QString getText();

    void setText(QString string);

public slots:
    int exec();

private slots:
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::InputDialog *ui;
};

#endif // INPUTDIALOG_H
