#ifndef ANIMATEDTEXTBROWSER_H
#define ANIMATEDTEXTBROWSER_H

#include <QMovie>
#include <QRegExp>
#include <QDebug>

#include "MyTextBrowser.h"

class AnimatedTextBrowser : public MyTextBrowser
{
    Q_OBJECT
public:
    AnimatedTextBrowser(QWidget* parent = 0);

    void addAnimation(const QUrl& url, const QString& fileName);

    void setHtml(const QString &text);

public slots:
    void append(const QString &text);

    void clear();

private slots:
    void animate();

    void disAnimate();

    void animateGif(const QString &text);

private:
    QHash<QMovie*, QUrl> urls;
    int i;
};

#endif // ANIMATEDTEXTBROWSER
