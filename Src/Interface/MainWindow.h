#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QKeyEvent>
#include <QTextBrowser>
#include <QSettings>

#include "../Game/Hero.h"
#include "../Core/OldDate.h"
#include "../Core/GameBase/ConvertToHtml.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Ui::MainWindow *ui;

    void addAction (const QString &string, short id, ConvertToHtml::Color color = ConvertToHtml::Color::Black, char font = 0);

public slots:

    void displayAdditionalDec();

private slots:
//    void on_textBrowser_anchorClicked(const QUrl &arg1);
    void editLevel(signed char level);
    void editMagikLevel(signed char magikLevel);

//    void on_actionSave_triggered();

protected:
    void changeEvent(QEvent *e);
    virtual bool eventFilter(QObject *pObject, QEvent *pEvent); //Перехватчик событий

private:
    QSet<int> keys;

    QSize bufSizeTextBrowser;

    QSettings settings;

    void readSettings();
    void writeSettings();

signals:
    void repaint();
};

#endif // MAINWINDOW_H
