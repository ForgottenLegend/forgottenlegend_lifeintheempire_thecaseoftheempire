#include "InputDialog.h"
#include "ui_InputDialog.h"

InputDialog::InputDialog(const QString &caption, const QString &text) :
//    QWidget(parent),
    ui(new Ui::InputDialog)
{
    ui->setupUi(this);

    this->setWindowTitle(caption);
//    connect(ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok),SIGNAL(clicked()), this, SLOT(accept()));
//    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
//        connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    ui->label->setText(text);
    static const int minWidth = 420;
    static const int maxWidth = 550;
    static const int minHeight = 150;
    static const int maxHeight = 350;

    setMinimumSize(minWidth, minHeight);
    setMaximumSize(maxWidth, maxHeight);

    ui->lineEdit->setFocus();
}

InputDialog::~InputDialog()
{
    delete ui;
}

QString InputDialog::getText()
{
    return ui->lineEdit->text();
}

void InputDialog::setText(QString string)
{
    ui->label->setText(string);
}

int InputDialog::exec()
{
    ui->lineEdit->clear();
    return QDialog::exec();
}

void InputDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    if (ui->buttonBox->standardButton(button) == QDialogButtonBox::Ok)
    {
//        QDialog::Accepted = QDialogButtonBox::Ok;
        accept();
    }
//        exit(ui->buttonBox->standardButton(button));
//        hide();
    else
        if (ui->buttonBox->standardButton(button) == QDialogButtonBox::Cancel)
            reject();
//    if (button->clicked();)
}
