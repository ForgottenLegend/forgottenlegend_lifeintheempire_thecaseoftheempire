#include "TableAction.h"

TableAction::TableAction(QWidget *parent) :
    QTableWidget(parent)
{
    connect(this, &TableAction::clicked, this, &TableAction::on_clicked);
}

void TableAction::on_clicked(QModelIndex index)
{
    if (this->item(index.row(),colomnId))
    {
        int buf = this->item(index.row(),colomnId)->text().toInt();
        emit actionClicked(buf);
    }
}
