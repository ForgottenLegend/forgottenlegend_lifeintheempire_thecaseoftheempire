#include "AnimatedTextBrowser.h"

AnimatedTextBrowser::AnimatedTextBrowser(QWidget *parent) :
    MyTextBrowser(parent)
{
}


void AnimatedTextBrowser::addAnimation(const QUrl &url, const QString &fileName)
{
    QMovie* movie = new QMovie(this);
    movie->setFileName(fileName);
    urls.insert(movie, url);
    connect(movie, SIGNAL(frameChanged(int)), this, SLOT(animate()));
    movie->start();
}

void AnimatedTextBrowser::setHtml(const QString &text)
{
    disAnimate();
    QTextBrowser::setHtml(text);
    animateGif(text);
}

void AnimatedTextBrowser::append(const QString &text)
{
    MyTextBrowser::append(text);
    animateGif(text);
}

void AnimatedTextBrowser::clear()
{
    MyTextBrowser::clear();
    disAnimate();
}

void AnimatedTextBrowser::animate()
{
    if (QMovie* movie = qobject_cast<QMovie*>(sender()))
    {
        document()->addResource(QTextDocument::ImageResource,
                                urls.value(movie), movie->currentPixmap());
        setLineWrapColumnOrWidth(lineWrapColumnOrWidth()); // causes reload
    }
}

void AnimatedTextBrowser::disAnimate()
{
    QHash<QMovie*, QUrl>::iterator i = urls.begin();
    for (; i != urls.end(); ++i)
    {
        disconnect(i.key(), SIGNAL(frameChanged(int)), this, SLOT(animate()));
        delete i.key();
    }
    urls.clear();
}

void AnimatedTextBrowser::animateGif(const QString &text)
{
    QString qs = text;
    if (qs.indexOf(".gif") > 0)
    {
        QRegExp qre("\"{0}[/.:\\w\\d\\s]+.gif");
        int pos = 0;
        while (pos >= 0)
        {
            pos = qre.indexIn(text, pos);
            if (pos >=0)
            {
                addAnimation(qre.cap(), qre.cap());
                pos += qre.cap().length();
            }
        }
    }
}
