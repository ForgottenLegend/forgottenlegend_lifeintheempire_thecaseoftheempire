#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "../Game/PreHistory.h"

AnimatedTextBrowser *textBrowser;

extern Hero *hero;
extern OldDate *date;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    settings(QCoreApplication::applicationDirPath () + "/settings.ini", QSettings::IniFormat)
{
    ui->setupUi(this);

    this->installEventFilter(this);
    ui->tableWidgetAction->installEventFilter(this);
    ui->textBrowser->installEventFilter(this);
    ui->textBrowserDec->installEventFilter(this);
    ui->tableWidgetObject->installEventFilter(this);

    ui->tableWidgetAction->hideColumn(2);

    textBrowser = ui->textBrowser;

    readSettings();

    ui->save->setEnabled(0);

    PreHistory *preHistory = new PreHistory(this, ui);
    preHistory->entry();

    connect(date, SIGNAL(changeHouring(long long)), this, SLOT(displayAdditionalDec()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

/*void MainWindow::on_textBrowser_anchorClicked(const QUrl &arg1)
{
    qDebug() << arg1.toString();
}*/

/*void MainWindow::on_actionSave_triggered()
{
    qDebug()<< 1;
}*/

void MainWindow::editLevel(signed char level)
{
    if (level > 0)
        ui->textBrowser->append(tr("ВАШ УРОВЕНЬ ПОВЫСИЛСЯ"));
    else
        if (level < 0)
            ui->textBrowser->append(tr("ВАШ УРОВЕНЬ ПОНИЗИЛСЯ"));

    ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
}

void MainWindow::editMagikLevel(signed char magikLevel)
{
    if (magikLevel > 0)
        ui->textBrowser->append(tr("ВАШ УРОВЕНЬ МАГИИ ПОВЫСИЛСЯ"));
    else
        if (magikLevel < 0)
            ui->textBrowser->append(tr("ВАШ УРОВЕНЬ МАГИИ ПОНИЗИЛСЯ"));

    ui->textBrowser->verticalScrollBar()->setValue(ui->textBrowser->verticalScrollBar()->maximum());
}

void MainWindow::addAction(const QString &string, short id, ConvertToHtml::Color color, char font)
{
    ui->tableWidgetAction->insertRow(ui->tableWidgetAction->rowCount());

    QColor qcolor;
    switch (color) {
    case ConvertToHtml::Color::Red:
        qcolor = QColor(255, 0, 0);
        break;
    case ConvertToHtml::Color::Green:
        qcolor = QColor(0, 255, 0);
        break;
    case ConvertToHtml::Color::Blue:
        qcolor = QColor(0, 0, 255);
        break;
    case ConvertToHtml::Color::Purple:
        qcolor = QColor(160, 32, 240);
        break;
    case ConvertToHtml::Color::Black:
        qcolor = QColor(0, 0, 0);
        break;
    default:
        break;
    }

    QTableWidgetItem *item = new QTableWidgetItem (string);
    delete ui->tableWidgetAction->item(ui->tableWidgetAction->rowCount() -1, ui->tableWidgetAction->colomnText);
    ui->tableWidgetAction->setItem(ui->tableWidgetAction->rowCount()-1, ui->tableWidgetAction->colomnText, item);

    QFont qfont = item->font();
    if (font & char(ConvertToHtml::Font::Italic))
        qfont.setItalic(1);
    if (font & char(ConvertToHtml::Font::Bold))
        qfont.setBold(1);
    if (font & char(ConvertToHtml::Font::Underline))
        qfont.setUnderline(1);

    item->setFont(qfont);
    item->setTextColor(qcolor);

    item = new QTableWidgetItem (QString::number(id));
    delete ui->tableWidgetAction->item(ui->tableWidgetAction->rowCount() -1, ui->tableWidgetAction->colomnId);
    ui->tableWidgetAction->setItem(ui->tableWidgetAction->rowCount()-1, ui->tableWidgetAction->colomnId, item);

    item->setTextColor(qcolor);
    item->setFont(qfont);

    item = new QTableWidgetItem (QString("[%0]").arg(ui->tableWidgetAction->rowCount()));
    delete ui->tableWidgetAction->item(ui->tableWidgetAction->rowCount() -1, 0);
    ui->tableWidgetAction->setItem(ui->tableWidgetAction->rowCount()-1, 0, item);

    item->setTextColor(qcolor);
    item->setFont(qfont);
}

void MainWindow::displayAdditionalDec()
{
    ui->textBrowserDec->clear();
    QString qs;
    qs.append(tr("%1:%2 %3: %4, %5, %6, %7 лето от З.И.И.Г.").
              arg(date->getHour()).arg(date->getHouring()).
              arg(date->getNameDayOfTheWeek()).arg(date->getDayOfTheSorokovnik() + 1).
              arg(date->getNameSorokovnik()).arg(date->getNameSeason()).arg(date->getYear()));
    int buf = hero->getHealth(), maxBuf = hero->getMaxHealth();
    double difBuf = buf / (double)maxBuf;
    qs.append("<br />");
    if (difBuf > 0.8)
        qs.append(ConvertToHtml::getColorStr(tr("Здоровье: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
    else
        if (difBuf > 0.5)
            qs.append(ConvertToHtml::getColorStr(tr("Здоровье: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
        else
            if (difBuf > 0.2)
                qs.append(ConvertToHtml::getColorStr(tr("Здоровье: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
            else
                qs.append(ConvertToHtml::getColorStr(tr("Здоровье: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));

    maxBuf = hero->getMaxMana();
    if (maxBuf > 0)
    {
        buf = hero->getMana();
        difBuf = buf / (double)maxBuf;

        qs.append("<br />");
        if (difBuf > 0.8)
            qs.append(ConvertToHtml::getColorStr(tr("Мана: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
        else
            if (difBuf > 0.5)
                qs.append(ConvertToHtml::getColorStr(tr("Мана: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
            else
                if (difBuf > 0.2)
                    qs.append(ConvertToHtml::getColorStr(tr("Мана: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
                else
                    qs.append(ConvertToHtml::getColorStr(tr("Мана: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));
    }

    buf = hero->getClearMind();
    maxBuf = hero->getMaxClearMind();
    difBuf = buf / (double)maxBuf;

    qs.append("<br />");
    if (difBuf > 0.8)
        qs.append(ConvertToHtml::getColorStr(tr("Чистота разума: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
    else
        if (difBuf > 0.5)
            qs.append(ConvertToHtml::getColorStr(tr("Чистота разума: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
        else
            if (difBuf > 0.2)
                qs.append(ConvertToHtml::getColorStr(tr("Чистота разума: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
            else
                qs.append(ConvertToHtml::getColorStr(tr("Чистота разума: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));


    buf = hero->getSatiety();
    maxBuf = hero->getMaxSatiety();
    difBuf = buf / (double)maxBuf;

    qs.append("<br />");
    if (difBuf > 0.8)
        qs.append(ConvertToHtml::getColorStr(tr("Сытость: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
    else
        if (difBuf > 0.5)
            qs.append(ConvertToHtml::getColorStr(tr("Сытость: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
        else
            if (difBuf > 0.2)
                qs.append(ConvertToHtml::getColorStr(tr("Сытость: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
            else
                qs.append(ConvertToHtml::getColorStr(tr("Сытость: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));

    buf = hero->getThirst();
    maxBuf = hero->getMaxThirst();
    difBuf = buf / (double)maxBuf;

    qs.append("<br />");
    if (difBuf > 0.8)
        qs.append(ConvertToHtml::getColorStr(tr("Жажда: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
    else
        if (difBuf > 0.5)
            qs.append(ConvertToHtml::getColorStr(tr("Жажда: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
        else
            if (difBuf > 0.2)
                qs.append(ConvertToHtml::getColorStr(tr("Жажда: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
            else
                qs.append(ConvertToHtml::getColorStr(tr("Жажда: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));

    buf = hero->getFatigue();
    maxBuf = hero->getMaxFatigue();
    difBuf = buf / (double)maxBuf;

    qs.append("<br />");
    if (difBuf > 0.8)
        qs.append(ConvertToHtml::getColorStr(tr("Силы: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
    else
        if (difBuf > 0.5)
            qs.append(ConvertToHtml::getColorStr(tr("Силы: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
        else
            if (difBuf > 0.2)
                qs.append(ConvertToHtml::getColorStr(tr("Силы: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
            else
                qs.append(ConvertToHtml::getColorStr(tr("Силы: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));

    buf = hero->getWill();
    maxBuf = hero->getMaxWill();
    difBuf = buf / (double)maxBuf;

    qs.append("<br />");
    if (difBuf > 0.8)
        qs.append(ConvertToHtml::getColorStr(tr("Сила воли: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Green));
    else
        if (difBuf > 0.5)
            qs.append(ConvertToHtml::getColorStr(tr("Сила воли: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Blue));
        else
            if (difBuf > 0.2)
                qs.append(ConvertToHtml::getColorStr(tr("Сила воли: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Purple));
            else
                qs.append(ConvertToHtml::getColorStr(tr("Сила воли: %1/%2").arg(buf).arg(maxBuf), ConvertToHtml::Color::Red));
    ui->textBrowserDec->setHtml(qs);
}

bool MainWindow::eventFilter(QObject *pObject, QEvent *pEvent)
{
    Q_UNUSED(pObject);
    if (pEvent->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = reinterpret_cast<QKeyEvent *>(pEvent);
        keys.insert(keyEvent->key());
        if (keys.count() == 2)
        {
            if (keys.contains(Qt::Key_Alt) || keys.contains(Qt::Key_AltGr))
                switch (keyEvent->key()) {
                case Qt::Key_1:
                    if (ui->tableWidgetAction->rowCount() >= 11)
                    {
                        if (ui->tableWidgetAction->item(10,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(10,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_2:
                    if (ui->tableWidgetAction->rowCount() >= 12)
                    {
                        if (ui->tableWidgetAction->item(11,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(11,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_3:
                    if (ui->tableWidgetAction->rowCount() >= 13)
                    {
                        if (ui->tableWidgetAction->item(12,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(12,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_4:
                    if (ui->tableWidgetAction->rowCount() >= 14)
                    {
                        if (ui->tableWidgetAction->item(13,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(13,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_5:
                    if (ui->tableWidgetAction->rowCount() >= 15)
                    {
                        if (ui->tableWidgetAction->item(14,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(14,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_6:
                    if (ui->tableWidgetAction->rowCount() >= 16)
                    {
                        if (ui->tableWidgetAction->item(15,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(15,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_7:
                    if (ui->tableWidgetAction->rowCount() >= 17)
                    {
                        if (ui->tableWidgetAction->item(16,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(16,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_8:
                    if (ui->tableWidgetAction->rowCount() >= 18)
                    {
                        if (ui->tableWidgetAction->item(17,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(17,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_9:
                    if (ui->tableWidgetAction->rowCount() >= 19)
                    {
                        if (ui->tableWidgetAction->item(18,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(18,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                case Qt::Key_0:
                    if (ui->tableWidgetAction->rowCount() >= 20)
                    {
                        if (ui->tableWidgetAction->item(19,ui->tableWidgetAction->colomnId))
                            emit ui->tableWidgetAction->actionClicked(
                                    ui->tableWidgetAction->item(19,ui->tableWidgetAction->colomnId)->text().toInt());
                    }
                    break;
                default:
                    break;
                }
//            emit ui->tableWidgetAction->clicked(index);
        }
//        qDebug() << keyEvent->key();
        else if (keys.count() == 1)
        {
            switch (keyEvent->key())
            {
            case Qt::Key_1:
                if (ui->tableWidgetAction->rowCount() >= 1)
                {
                    if (ui->tableWidgetAction->item(0,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(0,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_2:
                if (ui->tableWidgetAction->rowCount() >= 2)
                {
                    if (ui->tableWidgetAction->item(1,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(1,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_3:
                if (ui->tableWidgetAction->rowCount() >= 3)
                {
                    if (ui->tableWidgetAction->item(2,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(2,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_4:
                if (ui->tableWidgetAction->rowCount() >= 4)
                {
                    if (ui->tableWidgetAction->item(3,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(3,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_5:
                if (ui->tableWidgetAction->rowCount() >= 5)
                {
                    if (ui->tableWidgetAction->item(4,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(4,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_6:
                if (ui->tableWidgetAction->rowCount() >= 6)
                {
                    if (ui->tableWidgetAction->item(5,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(5,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_7:
                if (ui->tableWidgetAction->rowCount() >= 7)
                {
                    if (ui->tableWidgetAction->item(6,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(6,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_8:
                if (ui->tableWidgetAction->rowCount() >= 8)
                {
                    if (ui->tableWidgetAction->item(7,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(7,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_9:
                if (ui->tableWidgetAction->rowCount() >= 9)
                {
                    if (ui->tableWidgetAction->item(8,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(8,ui->tableWidgetAction->colomnId)->text().toInt());
                }
                break;
            case Qt::Key_0:
                if (ui->tableWidgetAction->rowCount() >= 10)
                {
                    if (ui->tableWidgetAction->item(9,ui->tableWidgetAction->colomnId))
                        emit ui->tableWidgetAction->actionClicked(
                                ui->tableWidgetAction->item(9,ui->tableWidgetAction->colomnId)->text().toInt());
                }
            default:
                break;
            }
        }
        return true;
    }
    else
        if (pEvent->type() == QEvent::KeyRelease)
        {
            QKeyEvent *keyEvent = reinterpret_cast<QKeyEvent *>(pEvent);
            keys.remove(keyEvent->key());
            return true;
        }
        else
            if (pEvent->type() == QEvent::Paint)
            {
                if (bufSizeTextBrowser != textBrowser->size())
                {
                    bufSizeTextBrowser = textBrowser->size();
                    emit repaint();
                }
            }
            else
                if (pEvent->type() == QEvent::Close)
                {
                    writeSettings();
                }
    return false;
}

void MainWindow::readSettings()
{
    QByteArray geometry, state;

    settings.beginGroup("/Settings");
        settings.beginGroup("/Form");
            geometry = settings.value("/geometry", saveGeometry()).toByteArray();
            state = settings.value("/state", saveState()).toByteArray();

            restoreGeometry(geometry);
            restoreState(state);
        settings.endGroup();
    settings.endGroup();
}

void MainWindow::writeSettings()
{
    settings.beginGroup("/Settings");
        settings.beginGroup("/Form");
            settings.setValue("/geometry", saveGeometry());
            settings.setValue("/state", saveState());
        settings.endGroup();
    settings.endGroup();
}
