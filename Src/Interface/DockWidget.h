#ifndef DOCKWIDGET_H
#define DOCKWIDGET_H

#include <QDockWidget>
#include <QTimer>
#include <QDebug>

class DockWidget : public QDockWidget
{
    Q_OBJECT
public:
    explicit DockWidget(const QString & title, QWidget *parent = 0, Qt::WindowFlags flags = 0);
    explicit DockWidget(QWidget *parent = 0, Qt::WindowFlags flags = 0);

    void resize(const QSize &size);
//    void setSize(const QSize &size);

//    QSize sizeHint() const;

signals:

public slots:

private slots:
    void returnToOldMaxMinSizes();

private:
    QSize oldMaxSize, oldMinSize;
};

#endif // DOCKWIDGET_H
