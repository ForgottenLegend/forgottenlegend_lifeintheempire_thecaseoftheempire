#ifndef MYTEXTBROWSER_H
#define MYTEXTBROWSER_H

#include <QTextBrowser>
#include <QDebug>

class MyTextBrowser : public QTextBrowser
{
    Q_OBJECT
public:
    explicit MyTextBrowser(QWidget *parent = 0);

signals:

public slots:

    void append(const QString &text);

    void insertHtml(const QString &text);
};

#endif // MYTEXTBROWSER_H
