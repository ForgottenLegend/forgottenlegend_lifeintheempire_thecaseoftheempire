#ifndef TABLEACTION_H
#define TABLEACTION_H

#include <QTableWidget>
#include <QDebug>

class TableAction : public QTableWidget
{
    Q_OBJECT
public:
    explicit TableAction(QWidget *parent = 0);

    enum {colomnText = 1, colomnId = 2};

signals:
    void actionClicked(int);

private slots:
    void on_clicked(QModelIndex index);

};

#endif // TABLEACTION_H
